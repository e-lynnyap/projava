package _03objects.P8_5;

/**
 * Created by elynn on 10/14/16.
 */
public class Driver {
    public static void main(String[] args) {
        // hard code values for radius and height
        double dRadius = 2.5;
        double dHeight = 5.5;

        // create sodacan with hard-coded values
        SodaCan sodCan = new SodaCan(dHeight, dRadius);

        System.out.println("For the values, height = " + dHeight + " and radius = " + dRadius + ":");

        // return sodacan surface area
        System.out.printf("%s%.2f.\n", "The surface area of your soda can (2dp) is ", sodCan.getSurfaceArea());

        // return sodacan volume
        System.out.printf("%s%.2f.\n", "The volume of your soda can (2dp) is ", sodCan.getVolume());

        // try to construct sodacan with negative values
        try {
            SodaCan sodTwo = new SodaCan(-4, 30);
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot construct soda can with negative arguments.");
        }
    }
}
