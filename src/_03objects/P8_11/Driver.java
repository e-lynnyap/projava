package _03objects.P8_11;

/**
 * Created by elynn on 10/15/16.
 */
public class Driver {
    public static void main(String[] args) {
        // hard-coded values for letter
        String strFrom = "Mary";
        String strTo = "John";
        String strLine1 = "I am sorry we must part.";
        String strLine2 = "I wish you all the best.";

        // construct a new letter
        Letter letOne = new Letter(strFrom, strTo);

        // add two lines to letter
        letOne.addLine(strLine1);
        letOne.addLine(strLine2);

        // print letter
        System.out.println(letOne.getText());

        // try to create a letter with empty strings
        try {
            Letter letTwo = new Letter("", "");
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot create letter with empty strings.");
        }

        // try to add an empty line to letter
        try {
            letOne.addLine("");
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot add empty line to letter.");
        }
    }
}
