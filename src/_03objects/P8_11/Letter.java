package _03objects.P8_11;

/**
 * Created by elynn on 10/15/16.
 */
public class Letter {

    private String mRecipient; // name of recipient
    private String mSender; // name of sender
    private String mBody = ""; // body of letter

    // constructor takes in names of sender and recipient as arguments
    public Letter(String strFrom, String strTo) {
        if (!validateString(strFrom) || !validateString(strTo)) {
            throw new IllegalArgumentException("Sender and recipient names must be valid, non-empty strings.");
        }
        mRecipient = strTo;
        mSender = strFrom;
    }

    // addLine adds a line of text to body of letter
    public void addLine(String strLine) {
        if (!validateString(strLine)) {
            throw new IllegalArgumentException("Line must be a valid, non-empty string.");
        }
        mBody += strLine + "\n";
    }

    // getText returns the entire text of the letter
    public String getText() {
        return header() + mBody + footer();
    }

    // helper method returns header of letter
    private String header() {
        return "Dear " + mRecipient + ":\n\n" ;
    }

    // helper method returns footer of letter
    private String footer() {
        return "\nSincerely,\n\n" + mSender;
    }

    // helper method validates string
    private Boolean validateString(String strInput) {
        return (strInput != null && !strInput.isEmpty());
    }
}
