package _03objects.P8_14;
import java.util.Scanner;

/**
 * Created by elynn on 10/16/16.
 */
public class Driver {
    public static void main(String[] args) {
        // create scanner object and read input
        Scanner scan = new Scanner(System.in);
        double dR;
        double dH;
        System.out.println("Please enter the value of the radius. Your number may include decimal places.");
        do {
            validateDouble(scan);
            dR = scan.nextDouble();
        } while (dR <= 0);
        System.out.println("Please enter the value of the height. Your number may include decimal places.");
        do {
            validateDouble(scan);
            dH = scan.nextDouble();
        } while (dH <= 0);

        // call methods and print results
        System.out.println("For the values height = " + dH + " and radius = " + dR + ":");
        System.out.print("The volume of a sphere (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", Geometry.sphereVolume(dR));
        System.out.print("The surface area of a sphere (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", Geometry.sphereSurface(dR));
        System.out.print("The volume of a cylinder (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", Geometry.cylinderVolume(dR, dH));
        System.out.print("The surface area of a cylinder (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", Geometry.cylinderSurface(dR, dH));
        System.out.print("The volume of a cone (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", Geometry.coneVolume(dR, dH));
        System.out.print("The surface area of a cone (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", Geometry.coneSurface(dR, dH));
    }

    // helper method to validate input from user
    private static void validateDouble(Scanner scan) {
        System.out.println("You must type in a positive number.");
        while (!scan.hasNextDouble()) {
            System.out.println("Invalid input. Enter a number.");
            scan.next();
        }
    }
}
