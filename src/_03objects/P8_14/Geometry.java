package _03objects.P8_14;

/**
 * Created by elynn on 10/16/16.
 */
public class Geometry {

    // calculate sphere volume
    public static double sphereVolume(double dR) {
        return ((4.0 / 3.0) * Math.PI * Math.pow(dR, 3));
    }

    // calculate sphere surface area
    public static double sphereSurface(double dR) {
        return (4.0 * Math.PI * dR * dR);
    }

    // calculate cylinder volume
    public static double cylinderVolume(double dR, double dH) {
        return (Math.PI * dR * dR * dH);
    }

    // calculate cylinder surface
    public static double cylinderSurface(double dR, double dH) {
        return (2.0 * Math.PI * dR * dH + 2.0 * Math.PI * dR * dR);
    }

    // calculate cone volume
    public static double coneVolume(double dR, double dH) {
        return (Math.PI * dR * dR * dH / 3.0);
    }

    // calculate cone surface
    public static double coneSurface(double dR, double dH) {
        return (Math.PI * dR * (dR + Math.sqrt(dH * dH + dR * dR)));
    }

}
