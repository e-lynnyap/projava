package _03objects.P8_15;

/**
 * Created by elynn on 10/16/16.
 */
public class Sphere {
    // declare instance variable for radius
    private double mR;

    // constructor takes in a single argument for radius
    public Sphere (double dR) {
        mR = dR;
    }

    // method to return volume
    public double getVolume() {
        return ((4.0 / 3.0) * Math.PI * Math.pow(mR, 3));
    }

    // method to return surface area
    public double getSurface() {
        return (4.0 * Math.PI * mR * mR);
    }
}
