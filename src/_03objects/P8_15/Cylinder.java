package _03objects.P8_15;

/**
 * Created by elynn on 10/16/16.
 */
public class Cylinder {
    // declare private instance variables for radius and height
    private double mR;
    private double mH;

    // constructor takes in two arguments for radius and height
    public Cylinder(double dR, double dH) {
        mR = dR;
        mH = dH;
    }

    // method to return volume
    public double getVolume() {
        return (Math.PI * mR * mR * mH);
    }

    // method to return surface area
    public double getSurface() {
        return (2.0 * Math.PI * mR * mH + 2.0 * Math.PI * mR * mR);
    }
}
