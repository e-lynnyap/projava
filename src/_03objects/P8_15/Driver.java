package _03objects.P8_15;

import java.util.Scanner;

/**
 * Answer to question ("Which approach is more object-oriented?"):
 * The approach in P8.15 is more object-oriented than P8.14, because it relies on separate classes for the various
 * geometrical objects and is therefore more modular than relying entirely on static methods. In this approach, we
 * only need to pass in the arguments once for each type of object, when we are instantiating their respective
 * classes. Thereafter, we need only call the instance methods on our newly-created objects in order to retrieve
 * the values we want. We do not have to pass in the values of the radius and height again, because when we
 * constructed the objects, their instance variables were initialized to these values. On the other hand, when we
 * use static methods entirely, we always have to pass in the arguments each time, because the values are not being
 * stored anywhere.
 */
public class Driver {
    public static void main(String[] args) {
        // create scanner object and read input
        Scanner scan = new Scanner(System.in);
        double dR;
        double dH;
        System.out.println("Please enter the value of the radius. Your number may include decimal places.");
        do {
            validateDouble(scan);
            dR = scan.nextDouble();
        } while (dR <= 0);
        System.out.println("Please enter the value of the height. Your number may include decimal places.");
        do {
            validateDouble(scan);
            dH = scan.nextDouble();
        } while (dH <= 0);

        // construct objects
        Sphere sphere = new Sphere(dR);
        Cylinder cylinder = new Cylinder(dR, dH);
        Cone cone = new Cone(dR, dH);

        // call methods and print results
        System.out.println("For the values height = " + dH + " and radius = " + dR + ":");
        System.out.print("The volume of a sphere (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", sphere.getVolume());
        System.out.print("The surface area of a sphere (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", sphere.getSurface());
        System.out.print("The volume of a cylinder (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", cylinder.getVolume());
        System.out.print("The surface area of a cylinder (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", cylinder.getSurface());
        System.out.print("The volume of a cone (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", cone.getVolume());
        System.out.print("The surface area of a cone (rounded to 2 decimal places) is: ");
        System.out.printf("%.2f\n", cone.getSurface());
    }


    // helper method to validate input from user
    private static void validateDouble(Scanner scan) {
        System.out.println("You must type in a positive number.");
        while (!scan.hasNextDouble()) {
            System.out.println("Invalid input. Enter a number.");
            scan.next();
        }
    }

}
