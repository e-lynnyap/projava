package _03objects.P8_15;

/**
 * Created by elynn on 10/16/16.
 */
public class Cone {
    // declare private instance variables for radius and height
    private double mR;
    private double mH;

    // constructor takes in two arguments for radius and height
    public Cone(double dR, double dH) {
        mR = dR;
        mH = dH;
    }

    // calculate cone volume
    public double getVolume() {
        return (Math.PI * mR * mR * mH / 3.0);
    }

    // calculate cone surface
    public double getSurface() {
        return (Math.PI * mR * (mR + Math.sqrt(mH * mH + mR * mR)));
    }
}
