package _03objects.P8_6;

/**
 * Created by elynn on 10/14/16.
 */
public class Driver {
    public static void main(String[] args) {

        // hard-coded values for Car methods
        int nEfficiency = 50;
        int nGas1 = 2;
        int nGas2 = 50;
        int nDistance1 = 100;
        int nDistance2 = 1000;

        // create Car object
        System.out.println("Creating a car with efficiency of " + nEfficiency + " miles/gallon.");
        Car carTest = new Car(nEfficiency);

        // add gas to car
        carTest.addGas(nGas1);
        System.out.println("After adding gas: ");
        printGasLevel(carTest);

        // drive car
        carTest.drive(nDistance1);
        printGasLevel(carTest);

        // try to drive car with not enough fuel
        System.out.println("Trying to drive another " + nDistance2 + " miles:");
        carTest.drive(nDistance2);

        // add gas to car
        carTest.addGas(nGas2);
        System.out.println("After adding gas: ");
        printGasLevel(carTest);

        // try to drive car again
        carTest.drive(nDistance2);

        // get gas level
        System.out.printf("%s%.2f%s\n", "There is ", carTest.getGasLevel(), " gallons of gas remaining.");

        // create Car object with negative efficiency; error will be thrown
        try {
            Car carSecond = new Car(-40);
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot create a car with negative efficiency.");
        }
    }

    public static void printGasLevel(Car car) {
        System.out.printf("%s%.2f%s\n", "There is ", car.getGasLevel(), " gallons of gas remaining.");
    }
}
