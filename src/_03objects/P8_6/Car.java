package _03objects.P8_6;

/**
 * Created by elynn on 10/14/16.
 */
public class Car {
    // declare private instance variables for efficiency and fuel
    private int mEfficiency;
    private double mFuel = 0; // fuel level is initially 0

    // constructor takes in efficiency as argument
    public Car(int nEfficiency) {
        if (nEfficiency < 0) {
            // throw error if user tries to construct object with negative argument
            throw new IllegalArgumentException("Car's efficiency must be a positive number.");
        } else {
            mEfficiency = nEfficiency;
        }
    }

    // public method drive that takes in an int as the distance driven and reduces fuel level
    public void drive(int nDistance) {
        // calculate the fuel required to drive this distance
        double dFuelRequired = nDistance * 1.0 / mEfficiency;

        if (dFuelRequired > mFuel) {
            // print message if distance exceeds distance permitted by current fuel level
            System.out.println("Need to add " + Math.round(dFuelRequired - mFuel) + " gallons of gas to drive "
                             + nDistance + " miles.");
        } else {
            System.out.println("Car just drove " + nDistance + " miles.");
            mFuel -= dFuelRequired; // subtract from fuel level
        }
    }

    // public method getGasLevel to return fuel remaining
    public double getGasLevel() {
        return mFuel;
    }

    // public method addGas to increase fuel level
    public void addGas(double dGas) {
        mFuel += dGas;
    }
}
