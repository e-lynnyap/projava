package _03objects.P8_9;

/**
 * Created by elynn on 10/15/16.
 */
public class Driver {
    public static void main(String[] args) {
        // construct a new combolock object
        ComboLock comLock = new ComboLock(10, 25, 30);

        // attempt 1: open combolock correctly
        System.out.println("Attempt 1: ");
        comLock.turnRight(30); // turn right from 0 to 10
        comLock.turnLeft(15); // turn left from 10 to 25
        comLock.turnRight(35); // turn right from 25 to 30
        printMessage(comLock.open());

        comLock.reset();

        // attempt 2: open combolock wrongly
        System.out.println("Attempt 2: ");
        comLock.turnRight(30); // turn right from 0 to 10
        comLock.turnLeft(16); // turn left from 10 to 26
        comLock.turnRight(35); // turn right from 26 to 31
        printMessage(comLock.open());

        // attempt 3: try to open combolock again, turning to correct values
        System.out.println("Attempt 3: ");
        comLock.turnRight(21); // turn right from 31 to 10
        comLock.turnLeft(15); // turn left from 10 to 25
        comLock.turnRight(35); // turn right from 25 to 30
        printMessage(comLock.open());

        comLock.reset();

        // attempt 4: doesn't matter how far left or right you turn the combolock as long as it stops at the right value
        System.out.println("Attempt 4: ");
        comLock.turnRight(70); // turn right from 0 to 10
        comLock.turnLeft(55); // turn left from 10 to 25
        comLock.turnRight(75); // turn right from 25 to 30
        printMessage(comLock.open());

        // try to turn lock by an invalid num of ticks
        try {
            comLock.turnLeft(-30);
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot turn lock by a negative number of ticks.");
        }

        // try to create lock with invalid secret values
        try {
            ComboLock comLock2 = new ComboLock(-30, 30, 39);
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot create combo lock with a negative secret.");
        }
        try {
            ComboLock comLock2 = new ComboLock(41, 30, 39);
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot create combo lock with a secret outside the range 0-39 inclusive.");
        }
    }

    // method prints success message if lock opened successfully, error otherwise
    public static void printMessage(Boolean bLockOpened) {
        if (bLockOpened) {
            System.out.println("Lock opened successfully.");
        } else {
            System.out.println("Lock did not open.");
        }
    }
}
