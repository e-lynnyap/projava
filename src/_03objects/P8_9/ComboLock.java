package _03objects.P8_9;

/**
 * Created by elynn on 10/15/16.
 */
public class ComboLock {

    private int mSecret1; // value of first num
    private int mSecret2; // value of second num
    private int mSecret3; // value of third num
    private int mDial;
    private Boolean mSecret1Unlocked = false; // becomes true when lock turned from 0 to mSecret1
    private Boolean mSecret2Unlocked = false; // true when mSecret1Unlocked and lock turned from mSecret1 to mSecret2
    private Boolean mSecret3Unlocked = false; // true when mSecret2Unlocked and lock turned from mSecret2 to mSecret3

    // constructor takes in three ints for the secret values
    public ComboLock(int nSecret1, int nSecret2, int nSecret3) {

        // validate input: all secret values must be between 0 and 39
        if (!(isValidSecret(nSecret1) && isValidSecret(nSecret2) && isValidSecret(nSecret3))) {
            throw new IllegalArgumentException("All secret values must be between 0 and 39 (inclusive).");
        }

        // validate input: in order for a lock to open, the dial must be TURNED from one num to a different num, so
        // any num in the sequence cannot be identical to an adjacent num
        if (nSecret2 == nSecret1) {
            throw new IllegalArgumentException("Secret 1 and Secret 2 must be different.");
        } else if (nSecret3 == nSecret2) {
            throw new IllegalArgumentException("Secret 3 and Secret 2 must be different.");
        }

        mSecret1 = nSecret1;
        mSecret2 = nSecret2;
        mSecret3 = nSecret3;
        reset(); // set dial to 0
    }

    // reset sets the dial to 0 and sets all the flags back to false
    public void reset() {
        mDial = 0;
        resetLock();
    }

    // turnRight takes in int for ticks turned and flags mSecret1Unlocked/mSecret3Unlocked
    public void turnRight(int nTicks) {
        validateTicks(nTicks);
        int nNewDialValue = (Math.floorMod(-nTicks, 40) + mDial) % 40; // number that dial has been turned to
        if (nNewDialValue == mSecret1 && !mSecret2Unlocked && !mSecret3Unlocked) {
            // if user turns right to mSecret1 at start of unlocking sequence
            mSecret1Unlocked = true;
        } else if (mDial == mSecret2 && nNewDialValue == mSecret3 && mSecret1Unlocked && mSecret2Unlocked) {
            // if user turns right from secret 2 to secret 3 and has unlocked 1 and 2 right before this
            mSecret3Unlocked = true;
        } else {
            resetLock();
        }
        mDial = nNewDialValue;
        System.out.println("Dial turned to " + mDial);
    }

    // turnLeft takes in int for ticks turned and flags mSecret2Unlocked
    public void turnLeft(int nTicks) {
        validateTicks(nTicks);
        int nNewDialValue = (mDial + nTicks) % 40;
        if (mDial == mSecret1 && nNewDialValue == mSecret2 && mSecret1Unlocked && !mSecret3Unlocked) {
            // if user turns right from secret 1 to secret 2 and has unlocked 1 right before this
            mSecret2Unlocked = true;
        }
        mDial = nNewDialValue;
        System.out.println("Dial turned to " + mDial);
    }

    // open will open lock if mSecret1Unlocked, mSecret2Unlocked and mSecret3Unlocked
    public boolean open() {
        if (mSecret1Unlocked && mSecret2Unlocked && mSecret3Unlocked) {
            resetLock();
            return true;
        } else {
            resetLock();
            return false;
        }
    }

    // helper method resets all flags to false if user does anything other than turn right from 0 to mSecret1 /
    // turn left from mSecret1 to mSecret2 / turn right from mSecret2 to mSecret3, or if lock is opened
    private void resetLock() {
        mSecret1Unlocked = false;
        mSecret2Unlocked = false;
        mSecret3Unlocked = false;
    }

    // helper method checks if secret value is in valid range
    private Boolean isValidSecret(int nSecret) {
        return (nSecret >= 0 && nSecret <= 39);
    }

    // helper method throws error if number of ticks turned is negative
    private void validateTicks(int nTicks) {
        if (nTicks < 0) {
            throw new IllegalArgumentException("Cannot turn lock with a negative number of ticks.");
        }
    }

}
