package _03objects.P8_4;

/**
 * Created by elynn on 10/14/16.
 */
public class Driver {
    public static void main(String[] args) {
        // create new Address objects
        Address addOne = new Address("45", "South Ellis Ave", "Chicago", "IL", "60615"); // without apt number
        Address addTwo = new Address("100A", "Sterling Place", "6C", "Brooklyn", "NY", "11238"); // with apt number

        // print and compare Address objects
        System.out.println("This address: ");
        addOne.print();
        if (addOne.comesBefore(addTwo)) {
            System.out.println("comes before: ");
        } else {
            System.out.println("does not come before: ");
        }
        addTwo.print();

        System.out.println();

        System.out.println("This address: ");
        addTwo.print();
        if (addTwo.comesBefore(addOne)) {
            System.out.println("comes before: ");
        } else {
            System.out.println("does not come before: ");
        }
        addOne.print();

        System.out.println();

        // cannot construct address objects with null or empty strings
        try {
            Address addThree = new Address("","","","","");
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot construct object with empty strings.");
        }
        try {
            Address addFour = new Address(null, null, null, null, null);
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot construct object with null references.");
        }
    }
}
