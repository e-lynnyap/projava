package _03objects.P8_4;

/**
 * Created by elynn on 10/14/16.
 */
public class Address {

    // declare instance variables
    // house number, street, apt num, city, state, postal code
    private String mHouse;
    private String mStreet;
    private String mApt;
    private String mCity;
    private String mState;
    private int mPostalCode;

    // first constructor: no apt num
    public Address(String strHouse, String strStreet, String strCity, String strState, String strPostalCode) {
        // validate all arguments first to ensure no null or empty strings
        if (!validateString(strHouse) || !validateString(strStreet) || !validateString(strCity) ||
                !validateString(strState) || !validateString(strPostalCode)) {
            throw new IllegalArgumentException("Arguments cannot be a null or empty string.");
        }

        mHouse = strHouse.trim(); // use trim to ensure no leading/trailing whitespaces (would affect output for print)
        mStreet = strStreet.trim();
        mCity = strCity.trim();
        mState = strState.trim();
        mPostalCode = Integer.parseInt(strPostalCode); // parameter must be string b/c zip might start with 0
        // since no apt num passed in, mApt will be initialized to default value i.e. null reference
    }

    // second constructor: with apt num
    public Address(String strHouse, String strStreet, String strApt, String strCity, String strState, String strPostalCode) {
        this(strHouse, strStreet, strCity, strState, strPostalCode); // call the first constructor

        // validate apt num
        if (!validateString(strApt)) {
            throw new IllegalArgumentException("Arguments cannot be a null or empty string.");
        }

        mApt = strApt.trim();
    }

    // public method to print address
    public void print() {
        // print street on one line
        System.out.print(mHouse + " " + mStreet);
        if (mApt != null) {
            System.out.print(" #" + mApt); // print apt number if object was constructed with one i.e. mApt not null
        }
        System.out.println();

        // print city, state, zip code on next line
        System.out.println(mCity + ", " + mState + " " + mPostalCode);
    }

    // public method returning true/false if object comes before argument in postal code
    // only true if postal code is strictly less than other
    // will return false if postal codes are the same
    public boolean comesBefore(Address other) {
        return this.mPostalCode < other.mPostalCode;
    }

    // helper method validates string
    private Boolean validateString(String strInput) {
        return (strInput != null && !strInput.isEmpty());
    }

}
