package _03objects.P8_8;

/**
 * Created by elynn on 10/14/16.
 * This program fulfills both P8.7 (quiz scores) and P8.8 (GPAs).
 */

public class Driver {
    public static void main(String[] args) {
        // hard-coded values for Student and Grade class methods
        String strName = "Ron"; // name for student
        int nQuiz1 = 76;
        int nQuiz2 = 97;
        int nQuiz3 = 36; // quiz scores
        String strGrade1 = "A+";
        String strGrade2 = "A-";
        String strGrade3 = "B+";
        String strGrade4 = "C+";
        String strInvalidGrade = "F-";

        // create a Student object
        System.out.println("Creating a new Student:");
        Student student = new Student(strName);

        // get name of student
        System.out.println("Student's name is " + student.getName());

        // add quizzes to student
        System.out.println("Adding quizzes to " + student.getName() + ": ");
        System.out.println(nQuiz1 + ", " + nQuiz2 + ", " + nQuiz3);
        student.addQuiz(nQuiz1);
        student.addQuiz(nQuiz2);
        student.addQuiz(nQuiz3);

        // get total score for student
        System.out.println(student.getName() + "'s total quiz score is " + student.getTotalScore());

        // get average score for student
        System.out.println(student.getName() + "'s average quiz score is " + student.getAverageScore());

        // create grades
        System.out.println("Adding grades to " + student.getName() + ": ");
        System.out.println(strGrade1 + ", " + strGrade2+ ", " + strGrade3 + ", " + strGrade4);
        Grade graOne = new Grade(strGrade1);
        Grade graTwo = new Grade(strGrade2);
        Grade graThree = new Grade(strGrade3);
        Grade graFour = new Grade(strGrade4);

        // call getter method for Grade to find string and numeric value
        System.out.println("The value of " + graOne.getGrade() + " is " + graOne.getValue());
        System.out.println("The value of " + graTwo.getGrade() + " is " + graTwo.getValue());
        System.out.println("The value of " + graThree.getGrade() + " is " + graThree.getValue());
        System.out.println("The value of " + graFour.getGrade() + " is " + graFour.getValue());

        // add grades to student
        student.addGrade(graOne);
        student.addGrade(graTwo);
        student.addGrade(graThree);
        student.addGrade(graFour);

        // get student's gpa
        System.out.println(student.getName() + "'s GPA is " + student.getGPA());

        // attempt to create a grade with an invalid string
        try {
            Grade graInvalid = new Grade(strInvalidGrade);
        } catch (IllegalArgumentException e) {
            System.out.println(strInvalidGrade + " is not a valid grade.");
        }

    }
}
