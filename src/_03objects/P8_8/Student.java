package _03objects.P8_8;

/**
 * Created by elynn on 10/14/16.
 */
public class Student {

    private String mName; // name of student
    private int mTotalQuiz; // total score for all quizzes
    private int mQuizCount = 0; // number of quizzes taken is initially 0
    private double mTotalGrades; // total numeric value for all grades
    private int mGradeCount = 0; // number of grades is initially 0

    // constructor takes a string as argument and initializes name
    public Student(String strName) {
        if (strName.isEmpty()) {
            throw new IllegalArgumentException("Must provide non-empty string for argument.");
        }
        mName = strName.trim();
    }

    // getName returns the name of the student
    public String getName() {
        return mName;
    }

    // addQuiz increases the total score by argument value and quiz count by 1
    public void addQuiz(int nScore) {
        mTotalQuiz += nScore;
        mQuizCount += 1;
    }

    // getTotalScore returns the total score
    public int getTotalScore() {
        return mTotalQuiz;
    }

    // getAverageScore returns the average score as a double rounded to one decimal place
    public double getAverageScore() {
        return Math.round(mTotalQuiz * 1.0 / mQuizCount * 10.0) / 10.0;
    }

    // addGrade increases total grade value by numeric value of grade and increments grade count by 1
    public void addGrade(Grade graGrade) {
        mTotalGrades += graGrade.getValue();
        mGradeCount += 1;
    }

    // getGPA returns the average grade
    public double getGPA() {
        return Math.round(mTotalGrades / mGradeCount * 100.0) / 100.0; // round gpa to 2 decimal places
    }

}
