package _03objects.P8_8;

/**
 * Created by elynn on 10/14/16.
 */
public class Grade {

    // declare instance variables for string and numeric value
    private String mGrade;
    private double mValue;

    // constructor takes in string as argument and calls helper method to calculate numeric value
    public Grade(String strGrade) {
        // validate argument to ensure that it is a valid grade
        try {
            mValue = calculateGradeValue(strGrade.trim());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Must provide a valid grade as argument. Valid grades are " +
                    "A+, A, A-, B+, B, B-, C+, C, F.");
        }
        mGrade = strGrade.trim();
    }

    // getValue returns the numeric value of the grade
    public double getValue() {
        return mValue;
    }

    // getGrade returns the name of the grade as a string
    public String getGrade() {
        return mGrade;
    }

    // helper method to convert string into numeric value
    private double calculateGradeValue(String strGrade) {
        switch (strGrade) {
            case "A+":
                return 4.0;
            case "A":
                return 4.0;
            case "A-":
                return 3.67;
            case "B+":
                return 3.33;
            case "B":
                return 3.0;
            case "B-":
                return 2.67;
            case "C+":
                return 2.33;
            case "C":
                return 2.0;
            case "F":
                return 0.0;
            default:
                // throw an error if user attempts to construct Grade with an invalid string
                throw new IllegalArgumentException("Must provide valid grade as argument.");
        }
    }

}
