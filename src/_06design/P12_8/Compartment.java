package _06design.P12_8;

/**
 * Created by elynn on 11/5/16.
 */
public class Compartment {
    private Product mProduct; // the type of product found in that compartment
    private int mQuantity; // the number of such products in the compartment

    private static final int MAX_QUANTITY = 10; // the maximum number of products a compartment can hold

    public Compartment() {
        mQuantity = 0;
        mProduct = null;
    }

    // adds a product in the quantity specified
    public void addProduct(Product product, int nQuantity) {
        mProduct = product;
        if (nQuantity > MAX_QUANTITY) { // if more products are added than is allowed, just add the max amt
            nQuantity = MAX_QUANTITY;
        }
        mQuantity = nQuantity;
    }

    // restocks a product
    public void restockProduct(int nQuantity) {
        int nNewQty = mQuantity + nQuantity;
        if (nNewQty > MAX_QUANTITY) {
            nNewQty = MAX_QUANTITY; // if more products are restocked than is allowed, just add the max amt
        }
        mQuantity = nNewQty;
    }

    public String getProductName() {
        return mProduct.getName();
    }

    public int getQuantity() {
        return mQuantity;
    }

    // if nothing is compartment, return 'empty'
    // else if anything is in compartment, returns a string with the name of the product and cost
    // else return a string with the name of the product and 'out of stock'
    @Override
    public String toString() {
        if (mProduct == null) {
            return "Empty";
        } else if (mQuantity > 0) {
            return mProduct.getName() + " - " + mProduct;
        } else {
            return mProduct.getName() + " - Out of Stock";
        }
    }

    // removes product from compartment and decreases quantity by 1
    public Product retrieveProduct() {
        mQuantity -= 1;
        return mProduct;
    }

    public double getProductCost() {
        return mProduct.getCost();
    }

    // no products were ever stocked in compartment
    public boolean isEmpty() {
        return (mProduct == null);
    }

    // products were stocked in compartment, but none left
    public boolean isOutOfStock() {
        return (mProduct != null && mQuantity == 0);
    }
}
