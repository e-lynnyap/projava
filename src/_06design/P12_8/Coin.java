package _06design.P12_8;

import java.text.NumberFormat;

/**
 * Created by elynn on 11/5/16.
 */
public class Coin {
    private double mValue;

    // set the value of the coin according to string passed into constructor
    public Coin(String strValue) {
        strValue = strValue.toLowerCase();

        switch (strValue) {
            case "q":
                mValue = 0.25;
                break;
            case "d":
                mValue = 0.10;
                break;
            case "n":
                mValue = 0.05;
                break;
            default:
                mValue = 0;
                break;
        }
    }

    public double getmValue() {
        return mValue;
    }

    public String toString() {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return formatter.format(mValue);
    }
}
