package _06design.P12_8;

import java.text.NumberFormat;

/**
 * Created by elynn on 11/5/16.
 */
public class Product {
    private String mName; // name of product
    private double mCost; // cost of product

    public Product(String mName, double mCost) {
        this.mName = mName;
        this.mCost = mCost;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public double getCost() {
        return mCost;
    }

    public void setCost(double mCost) {
        this.mCost = mCost;
    }

    public String toString() {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return formatter.format(mCost);
    }
}
