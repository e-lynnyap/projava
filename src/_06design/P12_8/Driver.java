package _06design.P12_8;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by elynn on 11/5/16.
 */
public class Driver {
    public static void main(String[] args) {

        VendingMachine vendingMachine = new VendingMachine();
        Scanner scan = new Scanner(System.in);

        // stock the vending machine with products and specify the quantity
        vendingMachine.stockProduct(new Product("Snickers", 1.00), 3);
        vendingMachine.stockProduct(new Product("Crunch Bar", 1.00), 3);
        vendingMachine.stockProduct(new Product("Haribo", 1.50), 2);
        vendingMachine.stockProduct(new Product("Kit-Kat", 1.25), 1);
        vendingMachine.stockProduct(new Product("Twizzlers", 0.75), 1);
        vendingMachine.stockProduct(new Product("Lays Potato Chips", 2.50), 2);
        vendingMachine.stockProduct(new Product("Trail Mix", 1.75), 2);
        vendingMachine.stockProduct(new Product("Cheez-Its", 3.00), 3);
        vendingMachine.stockProduct(new Product("Sun Chips", 2.25), 3);

        while (true) {
            String strChoice;
            do {
                System.out.println("Do you want to (i)nsert coins, (s)elect product, perform (o)perator actions, or (q)uit?");
                strChoice = scan.next().trim().toLowerCase();
                scan.nextLine(); // consume newline character
            } while (!strChoice.equals("i") && !strChoice.equals("s") && !strChoice.equals("q") && !strChoice.equals("o")); // validate user input

            if (strChoice.equals("i")) { // (i)nsert coins
                System.out.println("Please insert your coins in this format: q q d d n n");
                String strInput = scan.nextLine().toLowerCase();
                vendingMachine.insertCoins(strInput);
                System.out.println(vendingMachine.displayMoneyInserted());
            } else if (strChoice.equals("s")) { // (s)elect product
                System.out.println(vendingMachine.displaySelection());
                System.out.println(vendingMachine.displayMoneyInserted());
                System.out.println("Please choose the product you wish to purchase.");
                int nChoice;
                do {
                    System.out.println("You must enter a number between 1 and " + VendingMachine.NUM_OF_COMPARTMENTS);
                    validateInteger(scan);
                    nChoice = scan.nextInt();
                } while (invalidCompartment(nChoice)); // must be a valid choice of compartment
                try {
                    Product productReturned = vendingMachine.transact(nChoice);
                    System.out.println("Enjoy your purchase: " + productReturned.getName());
                    System.out.println(vendingMachine.displayMoneyInserted());
                } catch (InsufficientMoneyException ime) {
                    System.out.println("Insufficient money inserted to buy the product.");
                    returnMoney(vendingMachine);
                } catch (OutOfStockException ose) {
                    System.out.println("Product selected is out of stock.");
                    returnMoney(vendingMachine);
                } catch (IllegalArgumentException iae) {
                    System.out.println("Cannot select from an empty compartment.");
                    returnMoney(vendingMachine);
                }
            } else if (strChoice.equals("o")) { // (o)perator actions
                String strOperatorInput;
                do {
                    System.out.println("Do you want to (r)estock products or (g)et all the money from the machine?");
                    strOperatorInput = scan.next().trim().toLowerCase();
                    scan.nextLine(); // consume newline character
                } while (!strOperatorInput.equals("r") && !strOperatorInput.equals("g")); // validate user input
                if (strOperatorInput.equals("r")) { // (r)estock product
                    System.out.println("Here's how much of each product is in each compartment (note: you can only" +
                            " stock a maximum of 10 products in each compartment)");
                    System.out.println(vendingMachine.displayQuantities());
                    int nCompToRestock;
                    int nQty;
                    do {
                        System.out.println("Choose a valid compartment (1 to " + vendingMachine.NUM_OF_COMPARTMENTS + ") to restock.");
                        validateInteger(scan);
                        nCompToRestock = scan.nextInt();
                    } while (invalidCompartment(nCompToRestock));
                    do {
                        System.out.println("Enter a positive number for the quantity you want to restock.");
                        validateInteger(scan);
                        nQty = scan.nextInt();
                    } while (nQty < 1);
                    vendingMachine.restock(nCompToRestock, nQty);
                    System.out.println("Successfully restocked product.");
                } else if (strOperatorInput.equals("g")) { // (g)et all the money from the machine
                    System.out.println("Here's all the money in the machine: ");
                    ArrayList<Coin> coins = vendingMachine.emptyMachine();
                    printCoins(coins);
                }
            } else if (strChoice.equals("q")) {
                System.out.println("Thanks for using the vending machine.");
                return;
            }
        }
    }

    private static void returnMoney(VendingMachine vendingMachine) {
        System.out.println("Here is all your money back: ");
        ArrayList<Coin> coinsReturned = vendingMachine.returnCoins();
        printCoins(coinsReturned);
    }

    private static void printCoins(ArrayList<Coin> coins) {
        for (Coin coin : coins) {
            if (coin.getmValue() > 0) {
                System.out.println(coin);
            }
        }
    }

    private static void validateInteger(Scanner scan) {
        while (!scan.hasNextInt()) {
            System.out.println("Your answer must be a number.");
            scan.next();
        }
    }

    private static boolean invalidCompartment(int nChoice) {
        return (nChoice < 1 || nChoice > VendingMachine.NUM_OF_COMPARTMENTS);
    }
}
