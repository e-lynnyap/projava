package _06design.P12_8;

import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by elynn on 11/5/16.
 */
public class VendingMachine {
    public static final int NUM_OF_COMPARTMENTS = 9; // number of compartments in machine

    private ArrayList<Coin> mCoinsInserted = new ArrayList<>(); // coins currently inserted into vending machine
    private ArrayList<Coin> mBank = new ArrayList<>(); // coins in the vending machine's bank, retrievable by operator
    private Compartment[] compartments = new Compartment[NUM_OF_COMPARTMENTS]; // each compartment holds a quantity of a particular product


    public VendingMachine() {
        for (int i = 0; i < NUM_OF_COMPARTMENTS; i++) {
            compartments[i] = new Compartment();
        }
    }

    // stocks products in the first empty compartment
    public void stockProduct(Product product, int nQuantity) {
        // iterate through the compartments and find the first empty compartment
        // add products in the quantity specified to that first empty compartment
        for (int i = 0; i < NUM_OF_COMPARTMENTS; i++) {
            if (compartments[i].isEmpty()) {
                compartments[i].addProduct(product, nQuantity);
                return;
            }
        }
    }

    // displays selection
    public String displaySelection() {
        String strSelection = "";
        for (int i = 0; i < NUM_OF_COMPARTMENTS; i++) {
            strSelection += "(" + (i + 1) + ") " + compartments[i] + "\n";
        }
        return strSelection;
    }

    // displays the amount of money currently inserted
    public String displayMoneyInserted() {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return "Credit: " + formatter.format(countCoins());
    }

    // adds coins to the vending machine
    public void insertCoins(String strInput) {
        String[] strCoins = strInput.split(" ");
        for (String strCoin : strCoins) {
            mCoinsInserted.add(new Coin(strCoin));
        }
    }

    // returns all coins
    public ArrayList<Coin> returnCoins() {
        ArrayList<Coin> coinsToReturn = mCoinsInserted;
        mCoinsInserted = new ArrayList<>();
        return coinsToReturn;
    }

    // returns the product selected if there are sufficient coins inserted
    public Product transact(int nChoice) throws OutOfStockException, InsufficientMoneyException {
        int nCompartment = nChoice - 1; // subtract 1 to access correct element in array

        if (compartments[nCompartment].isEmpty()) {
            throw new IllegalArgumentException(); // cannot select from an empty compartment
        } else if (compartments[nCompartment].isOutOfStock()) {
            throw new OutOfStockException(); // product is out of stock
        } else if (compartments[nCompartment].getProductCost() > countCoins()) {
            throw new InsufficientMoneyException(); // insufficient money to purchase product
        }

        transferCoinsToBank();
        return compartments[nCompartment].retrieveProduct();
    }

    // restocks the product in the compartment, by the specified quantity
    public void restock(int nChoice, int nQty) {

        if (nChoice < 1 || nChoice > NUM_OF_COMPARTMENTS) {
            throw new IllegalArgumentException("Not a valid compartment.");
        }

        int nCompartment = nChoice - 1; // subtract 1 to access correct element in array
        compartments[nCompartment].restockProduct(nQty);
    }

    // shows the quantities of each product in each compartment
    public String displayQuantities() {
        String strQuantities = "";
        for (int i = 0; i < NUM_OF_COMPARTMENTS; i++) {
            strQuantities += "(" + (i + 1) + ") " + compartments[i].getProductName() + " - " + compartments[i].getQuantity() + "\n";
        }
        return strQuantities;
    }

    // returns all the money in the machine
    public ArrayList<Coin> emptyMachine() {
        transferCoinsToBank();
        ArrayList<Coin> bankMoney = mBank;
        mBank = new ArrayList<>();
        return bankMoney;
    }

    // counts all the money inserted
    private double countCoins() {
        double nAmount = 0;
        for (Coin coin : mCoinsInserted) {
            nAmount += coin.getmValue();
        }
        return nAmount;
    }

    // transfers coins inserted to the machine bank
    private void transferCoinsToBank() {
        mBank.addAll(mCoinsInserted);
        mCoinsInserted = new ArrayList<>(); // available credit after transfer is $0.00
    }
}
