package _06design.P12_6;

import java.util.Random;

/**
 * Creates a question that tests addition of numbers less than 10 whose sum is less than 10. Numbers must be at least 1.
 */
public class Level1Question extends Question {

    public Level1Question() {
        Random rand = new Random();
        int nOperand1 = rand.nextInt(8) + 1; // first operand must be a number between 1 and 8, inclusive
        int nOperand2 = rand.nextInt(9 - nOperand1) + 1; // sum of operands must not exceed 9
        setQuestion("What is " + nOperand1  + " + " + nOperand2 + "?"); // sets question
        setAnswer(nOperand1 + nOperand2); // sets answer
    }
}
