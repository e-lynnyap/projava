package _06design.P12_6;

import java.util.Random;

/**
 * Creates a question that tests subtraction of one-digit numbers with a non-negative difference. Numbers must be at
 * least 1.
 */
public class Level3Question extends Question {

    public Level3Question() {
        Random rand = new Random();
        int nOperand1 = rand.nextInt(9) + 1; // first operand must be a number between 1 and 9, inclusive
        int nOperand2 = rand.nextInt(nOperand1) + 1; // second operand must be a number between 1 and first operand
        setQuestion("What is " + nOperand1  + " - " + nOperand2 + "?"); // sets question
        setAnswer(nOperand1 - nOperand2); // sets answer
    }
}
