package _06design.P12_6;

/**
 * Created by elynn on 11/5/16.
 */
public class Question {

    private String mQuestion;
    private int mAnswer;

    // prints out the question
    public void display() {
        System.out.println(mQuestion);
    }

    // checks answer and returns true if correct, false otherwise
    public boolean checkAnswer(int nAnswer) {
        return mAnswer == nAnswer;
    }

    public void setQuestion(String mQuestion) {
        this.mQuestion = mQuestion;
    }

    public int getAnswer() {
        return mAnswer;
    }

    public void setAnswer(int mAnswer) {
        this.mAnswer = mAnswer;
    }
}
