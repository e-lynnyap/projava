package _06design.P12_6;

import java.util.Scanner;

/**
 * Creates a game that tests arithmetic questions and advances player to the next level if enough points are earned.
 */
public class Game {

    private int mLevel = 1; // game starts at level 1
    private int mPoints = 0; // number of points is initially 0
    private Question mCurrentQuestion; // question currently being asked
    private int mAnswer; // player's answer to current question
    private Scanner scan;

    private static final int MAX_LEVEL = 3; // highest level that can be reached
    private static final int POINTS_TO_ADVANCE = 5; // points needed to advance to next level
    private static final int TRIES_PER_QUESTION = 2; // number of tries player gets per question

    public void play() {
        scan = new Scanner(System.in);

        while (true) {
            System.out.println("Welcome to the arithmetic game!");
            outer:
            while (mLevel <= MAX_LEVEL) { // loop iterates as long as player has not passed max level
                while (mPoints < POINTS_TO_ADVANCE) {
                    // ask a question
                    // initialize number of tries to 0
                    // while number of tries does not exceed max
                        // if answer is correct
                            // add one point
                        // else
                            // decrease tries left

                    mCurrentQuestion = createQuestion(mLevel);
                    int nTries = 0;
                    while (nTries < TRIES_PER_QUESTION) {
                        askCurrentQuestion();
                        if (mCurrentQuestion.checkAnswer(mAnswer)) {
                            System.out.println("Correct answer!");
                            mPoints += 1;
                            break;
                        } else {
                            nTries += 1;
                            System.out.println("Wrong answer.");
                            if (nTries < TRIES_PER_QUESTION) {
                                System.out.println("Try again.");
                            }
                        }
                    }

                    // if player has hit max tries
                        // print message and ask player if they want to restart
                        // if player wants to restart
                            // reset points to 0 and level to 1 and restart game
                        // else
                            // return from method
                    // else
                        // print message with current points

                    if (nTries == TRIES_PER_QUESTION) {
                        System.out.println("Sorry, you have no more tries left.");
                        String strAnswer;
                        do {
                            System.out.println("Do you want to (r)estart the game, or (q)uit?");
                            strAnswer = scan.next().trim().toLowerCase();
                        } while (!strAnswer.equals("r") && !strAnswer.equals("q")); // validate user input
                        if (strAnswer.equals("r")) {
                            mPoints = 0;
                            mLevel = 1;
                            break outer;
                        } else {
                            System.out.println("Thanks for playing.");
                            return;
                        }
                    } else {
                        System.out.println("Current score for Level " + mLevel + ": " + mPoints);
                        System.out.println();
                    }

                }

                // increase current level by 1
                // reset points to 0
                // if there are still levels left
                // print advancing message
                // else if player has finished last level
                // print success message

                mLevel += 1;
                mPoints = 0;
                if (mLevel <= MAX_LEVEL) {
                    System.out.println("Advancing to Level " + mLevel + "!");
                    System.out.println();
                } else if (mLevel > MAX_LEVEL) {
                    System.out.println("Congratulations, you've completed the game!");
                    return;
                }
            }
        }


    }

    // displays the current question and gets the player's answer
    private void askCurrentQuestion() {
        mCurrentQuestion.display();
        while (!scan.hasNextInt()) {
            System.out.println("Your answer must be a number.");
            scan.next();
        }
        mAnswer = scan.nextInt();
    }

    // create a question of the appropriate level, according to what level the player is currently at
    private Question createQuestion(int nLevel) {
        if (nLevel == 1) {
            return new Level1Question();
        } else if (nLevel == 2) {
            return new Level2Question();
        } else {
            return new Level3Question();
        }
    }
}
