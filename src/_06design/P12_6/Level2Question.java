package _06design.P12_6;

import java.util.Random;

/**
 * Creates a question that tests addition of arbitrary one-digit numbers between 1 and 9 inclusive.
 */
public class Level2Question extends Question {

    public Level2Question() {
        Random rand = new Random();
        int nOperand1 = rand.nextInt(9) + 1; // number must be between 1 and 9
        int nOperand2 = rand.nextInt(9) + 1;
        setQuestion("What is " + nOperand1  + " + " + nOperand2 + "?"); // sets question
        setAnswer(nOperand1 + nOperand2); // sets answer
    }
}
