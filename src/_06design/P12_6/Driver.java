package _06design.P12_6;

/**
 * Created by elynn on 11/5/16.
 */
public class Driver {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}
