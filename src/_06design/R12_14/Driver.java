package _06design.R12_14;

import java.io.File;
import java.util.ArrayList;

/**
 * A program that reads a file containing a list of countries to find the ones with the largest area/population/density.
 */
public class Driver {

    private ArrayList<Country> countries;

    /**
     * Reads a file containing a list of records describing countries and adds each country to an array list.
     * @param input a set of records describing countries. Each record consists of the name of the country,
     *              its population, and its area.
     */
    public void readFile(File input) {

    }

    /**
     * Adds a country to the list of countries to be compared with each other.
     * @param country a country to be added to the list
     */
    public void addCountry(Country country) {
        countries.add(country);
    }

    /**
     * Finds the country with the largest area.
     * @return the country with the largest area.
     */
    public Country findCountryWithLargestArea() {
        return new Country(0,0,"");
    }

    /**
     * Finds the country with the largest population.
     * @return the country with the largest population.
     */
    public Country findCountryWithLargestPopulation() {
        return new Country(0,0,"");
    }

    /**
     * Finds the country with the largest population density.
     * @return the country with the largest population density.
     */
    public Country findCountryWithLargestDensity() {
        return new Country(0,0,"");
    }


}
