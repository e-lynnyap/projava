package _06design.R12_14;

/**
    A country with a name, area and population size.
 */
public class Country {
    private double mArea;
    private int mPopulation;
    private String mName;

    /**
     * Constructs a country.
     * @param dArea the area of the country.
     * @param nPopulation the population of the country.
     * @param strName the name of the country.
     */
    public Country(double dArea, int nPopulation, String strName) {
        mArea = dArea;
        mPopulation = nPopulation;
        mName = strName;
    }

    /**
     * Gets the area of the country.
     * @return the area of the country.
     */
    public double getmArea() {
        return mArea;
    }

    /**
     * Gets the population of the country.
     * @return the population of the country.
     */
    public int getmPopulation() {
        return mPopulation;
    }

    /**
     * Gets the population density of the country.
     * @return the population density of the country.
     */
    public double getDensity() {
        return mPopulation / mArea;
    }

    /**
     * Gets the name of the country.
     * @return the name of the country.
     */
    public String getmName() {
        return mName;
    }
}
