package _07blackjack.blackjack;

import java.util.ArrayList;

/**
 * Creates a standard deck of 52 cards
 */
public class Deck {
    private ArrayList<Card> cards = new ArrayList<>(); // holds the cards that are currently in the deck

    // creates a deck of cards with info about suit, rank, and image filename
    public Deck() {
        for (int nSuit = 0; nSuit < 4; nSuit++) {
            for (int nRank = 0; nRank < 13; nRank++) {
                cards.add(new Card(nSuit, nRank, nSuit + "-" + nRank + ".png"));
            }
        }
    }

    // returns all the cards in the deck as an arraylist
    public ArrayList<Card> getCards() {
        return cards;
    }
}
