package _07blackjack.blackjack;

import java.util.ArrayList;
import java.util.Random;

/**
 * Creates a shoe holding one or more decks.
 */
public class Shoe {
    static final int NUM_OF_DECKS = 6; // 6-deck shoe
    private ArrayList<Card> cards = new ArrayList<>(); // all the cards in the shoe

    public Shoe() {
        for (int i = 0; i < NUM_OF_DECKS; i++) {
            cards.addAll(new Deck().getCards());
        }
    }

    // selects a random card from the shoe, removes it, and returns it
    public Card dealCard() {
        return cards.remove(new Random().nextInt(cards.size()));
    }
}
