package _07blackjack.blackjack;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Creates a blackjack game with two players.
 */
public class Game {
    public static final int LIMIT = 21; // max legal value of hand

    private static final int FRAME_WIDTH = 800; // dimensions of game frame
    private static final int FRAME_HEIGHT = 550;

    private static final int INITIAL_MONEY = 1000; // the amount of money the player starts with

    private static final String RULES = "1. You must place a valid bet before the round can start. \n" +
            "2. Surrendering gives you half your bet back. \n" +
            "3. Doubling down doubles your bet and commits you to receiving exactly one more card. \n" +
            "4. You can only double down or surrender on your first two cards. \n" +
            "5. Bets are subtracted from your balance upon the start of the round. \n" +
            "6. If you win, you get your original bet back + the bet amount. \n" +
            "7. If the result is a push, you get your original bet back. \n" +
            "8. If you lose, you do not get your bet back. \n" +
            "9. If balance goes below $2, you cannot play anymore. \n" +
            "10. Dealer hits on soft 17. \n" +
            "11. Bets cannot be changed after cards are dealt, except by doubling down. \n" +
            "12. You cannot bet more than your balance (i.e. you cannot owe the dealer money).";

    private Shoe shoe;
    private Player player;
    private Player dealer;

    private int betAmount; // current bet
    private int balanceAmount;

    private JFrame frame;

    private JPanel dealerPanel; // contains dealer's cards
    private JPanel playerPanel; // contains player's cards
    private JPanel betPanel; // contains betting buttons
    private JPanel buttonPanel; // contains gameplay buttons
    private JPanel gamePanel; // container for all buttons and status messages
    private JPanel rulesPanel; // contains button to view game rules

    private JLabel message; // reflects current status
    private JLabel balance; // amount of money that player has left
    private JLabel bet; // amount that is currently being bet
    private JLabel betInstruct; // tells player to set their bet before dealing

    private JButton bet2; // buttons to increase bet
    private JButton bet10;
    private JButton bet20;
    private JButton bet50;
    private JButton resetBet; // resets bet to 0
    private JButton doubleButton; // to double bet after getting first 2 cards
    private JButton surrenderButton; // to surrender after getting first 2 cards
    private JButton hitButton; // to take one more card
    private JButton standButton; // to stop taking cards
    private JButton dealButton; // to start new round
    private JButton rulesButton; // to display pop-up with rules of game

    public Game() {
        player = new Player("player");
        dealer = new Player("dealer");
        frame = new JFrame("Blackjack");
        betAmount = 0;
        balanceAmount = INITIAL_MONEY;
    }

    public void start() {
        constructComponents();
        displayFrame();
        roundInProgress(false);
    }

    // constructs swing components for game
    private void constructComponents() {
        dealerPanel = new JPanel();
        playerPanel = new JPanel();
        betPanel = new JPanel();
        buttonPanel = new JPanel();
        gamePanel = new JPanel();
        rulesPanel = new JPanel();

        dealerPanel.setBackground(Color.DARK_GRAY);
        playerPanel.setBackground(Color.DARK_GRAY);

        message = new JLabel("");
        balance = new JLabel("");
        bet = new JLabel("");

        constructBetButtons();
        constructHitButton();
        constructStandButton();
        constructDealButton();
        constructSurrenderButton();
        constructDoubleButton();
        constructRulesButton();

        betPanel.add(betInstruct);
        betPanel.add(bet2);
        betPanel.add(bet10);
        betPanel.add(bet20);
        betPanel.add(bet50);
        betPanel.add(resetBet);
        buttonPanel.add(doubleButton);
        buttonPanel.add(surrenderButton);
        buttonPanel.add(hitButton);
        buttonPanel.add(standButton);
        buttonPanel.add(dealButton);
        message.setHorizontalAlignment(JLabel.CENTER);
        message.setForeground(Color.BLUE);
        balance.setHorizontalAlignment(JLabel.CENTER);
        bet.setHorizontalAlignment(JLabel.CENTER);
        rulesPanel.add(new JLabel(""));
        rulesPanel.add(rulesButton);

        gamePanel.setLayout(new GridLayout(6, 1));
        gamePanel.add(message);
        gamePanel.add(balance);
        gamePanel.add(bet);
        gamePanel.add(betPanel);
        gamePanel.add(buttonPanel);
        gamePanel.add(rulesPanel);

        frame.add(dealerPanel, BorderLayout.NORTH);
        frame.add(playerPanel, BorderLayout.CENTER);
        frame.add(gamePanel, BorderLayout.SOUTH);

        updateBalance();
        updateBet();
    }

    // when clicked, produces a pop-up that displays the rules of the game
    private void constructRulesButton() {
        rulesButton = new JButton("View Rules");
        rulesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                JOptionPane.showMessageDialog(frame, RULES, "Blackjack Rules", JOptionPane.PLAIN_MESSAGE);
            }
        });
    }

    // when clicked, doubles the current bet and hits the player with exactly one more card before dealer draws
    private void constructDoubleButton() {
        doubleButton = new JButton("Double down");
        doubleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hit(player);
                balanceAmount -= betAmount; // deduct bet from balance again
                updateBalance(); // show the new balance
                betAmount *= 2; // double the bet
                updateBet(); // show the new bet
                if (!player.isBusted()) {
                    dealerTurn(); // dealer draws only if the player's additional card doesn't bust the player
                }
                showRoundResults();
            }
        });
    }

    private void showRoundResults() {
        message.setText(gameResult());
        showDealerCards();
        roundInProgress(false);
        checkBalance();
    }

    // if true, then user can only click on gameplay buttons (hit, stand, surrender, double down)
    // if false, then user can only click on betting buttons + deal
    private void roundInProgress(boolean isInProgress) {
        betInstruct.setVisible(!isInProgress);
        bet2.setEnabled(!isInProgress); // if round in progress, cannot change bet
        bet10.setEnabled(!isInProgress);
        bet20.setEnabled(!isInProgress);
        bet50.setEnabled(!isInProgress);
        resetBet.setEnabled(!isInProgress);
        dealButton.setEnabled(!isInProgress);

        if (!isInProgress) {
            validateBetButtons();
            if (betAmount > 0 && betAmount <= balanceAmount) {
                dealButton.setEnabled(true); // can only deal cards if bet is positive and less than balance
            } else {
                dealButton.setEnabled(false);
            }
        }

        hitButton.setEnabled(isInProgress);
        standButton.setEnabled(isInProgress);
        firstRoundButtonsEnabled(isInProgress);
    }

    // when clicked, half of bet is returned to the user and the round ends
    private void constructSurrenderButton() {
        surrenderButton = new JButton("Surrender");
        surrenderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int amountReturned = betAmount / 2;
                balanceAmount += amountReturned;
                updateBalance();
                message.setText("Surrender. You get $" + amountReturned + " back.");
                showDealerCards();
                roundInProgress(false);
                checkBalance();
            }
        });
    }

    // when clicked, first two cards are dealt to the player and dealer each
    private void constructDealButton() {
        dealButton = new JButton("Deal");
        dealButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                player.discardHand();
                dealer.discardHand();

                // clear screen for new cards
                playerPanel.removeAll();
                dealerPanel.removeAll();

                shoe = new Shoe(); // reset shoe

                message.setText(""); // clear message from previous round

                balanceAmount -= betAmount; // subtract bet from balance
                updateBalance();

                roundInProgress(true);
                dealButton.setEnabled(false); // cannot deal again until round ends

                // deal the first two cards to each player
                hit(player);
                hit(player);
                hit(dealer, true); // this card is face-down
                hit(dealer);
            }
        });
    }

    // when clicked, the player stops drawing cards and the dealer's turn begins
    private void constructStandButton() {
        standButton = new JButton("Stand");
        standButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dealerTurn();
                showRoundResults();
            }
        });
    }

    // add cards to the dealer's hand
    private void dealerTurn() {
        while (dealer.getValue() < 17 || dealer.soft17()) { // dealer hits if value below 17 or hand is soft 17
            hit(dealer);
        }
    }

    // when clicked, add a card to the player's hand
    private void constructHitButton() {
        hitButton = new JButton("Hit");
        hitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firstRoundButtonsEnabled(false); // cannot double down or surrender after hitting once
                hit(player);
                if (player.isBusted()) { // round ends once player busts
                    showRoundResults();
                }
            }
        });
    }

    // buttons to set the bet before dealing cards
    private void constructBetButtons() {
        class betListener implements ActionListener {
            private int mBet;

            public betListener(int nBet) {
                mBet = nBet;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                increaseBet(mBet);
                roundInProgress(false);
            }
        }

        betInstruct = new JLabel("Set your bet: ");
        betInstruct.setForeground(Color.RED);

        bet2 = new JButton("+$2");
        bet10 = new JButton("+$10");
        bet20 = new JButton("+$20");
        bet50 = new JButton("+$50");

        resetBet = new JButton("Reset bet");

        bet2.addActionListener(new betListener(2));
        bet10.addActionListener(new betListener(10));
        bet20.addActionListener(new betListener(20));
        bet50.addActionListener(new betListener(50));

        resetBet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                increaseBet(-betAmount);
                roundInProgress(false);
            }
        });
    }

    // displays the frame
    private void displayFrame() {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setVisible(true);
    }

    // adds card to player's hand and updates player panel, showing card face-down if hidden and face-up otherwsie
    private void hit(Player player, Boolean isHidden) {
        JPanel panel = new JPanel();

        if (player.toString().equals("player")) {
            panel = playerPanel;
        } else if (player.toString().equals("dealer")) {
            panel = dealerPanel;
        }

        Card card = shoe.dealCard();
        player.addCard(card);

        try {
            if (isHidden) { // show card as face-down
                panel.add(new JLabel(new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream("images/card-back.png")))));
            } else { // show card as face-up
                panel.add(new JLabel(new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream("images/" + card)))));
            }
        } catch (IOException e) {
            System.out.println("Image not found.");
        }

        panel.updateUI();
    }

    // shows the card face-up
    private void hit(Player player) {
        hit(player, false);
    }

    // checks to see who won the game
    private String gameResult() {
        if (player.isBusted()) {
            return "You busted!";
        } else if (dealer.isBusted()) {
            increaseWinBalance();
            return "Dealer busted. You get $" + betAmount + " back plus another $" + betAmount + "!";
        } else if (player.getValue() == dealer.getValue()) {
            increasePushBalance();
            return "Push. You get $" + betAmount + " back.";
        } else if (player.getValue() > dealer.getValue()) {
            increaseWinBalance();
            return "You win. You get $" + betAmount + " back plus another $" + betAmount + "!";
        } else {
            return "Dealer won!";
        }
    }

    // enables the double down and surrender buttons if true, disables them if false
    private void firstRoundButtonsEnabled(boolean isEnabled) {
        surrenderButton.setEnabled(isEnabled);
        doubleButton.setEnabled(isEnabled);
        if (isEnabled && balanceAmount < betAmount) {
            doubleButton.setEnabled(false); // cannot double down if insufficient balance
        }
    }

    // show all dealer's cards as face-up
    private void showDealerCards() {
        dealerPanel.removeAll();
        for (String card : dealer.getCards()) {
            try {
                dealerPanel.add(new JLabel(new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream("images/" + card)))));
            } catch (IOException e) {
                System.out.println("Image not found.");
            }
        }
    }

    // increase bet and display updated amount
    private void increaseBet(int nAmount) {
        betAmount += nAmount;
        updateBet();
    }

    // disable the bet buttons that would push the bet amount over the current balance
    private void validateBetButtons() {
        if (betAmount + 2 > balanceAmount) {
            bet2.setEnabled(false);
            bet10.setEnabled(false);
            bet20.setEnabled(false);
            bet50.setEnabled(false);
        } else if (betAmount + 10 > balanceAmount) {
            bet10.setEnabled(false);
            bet20.setEnabled(false);
            bet50.setEnabled(false);
        } else if (betAmount + 20 > balanceAmount) {
            bet20.setEnabled(false);
            bet50.setEnabled(false);
        } else if (betAmount + 50 > balanceAmount) {
            bet50.setEnabled(false);
        }
    }

    // increase balance if player won
    private void increaseWinBalance() {
        balanceAmount += betAmount * 2; // win back bet plus bet amount
        updateBalance();
    }

    // increase balance if result was push
    private void increasePushBalance() {
        balanceAmount += betAmount;
        updateBalance();
    }

    // display the updated balance amount
    private void updateBalance() {
        balance.setText("Balance: $" + balanceAmount);
    }

    // display the updated bet amount
    private void updateBet() {
        bet.setText("Current Bet: $" + betAmount);
    }

    // display prompt to restart if balance is insufficient to keep playing
    private void checkBalance() {
        if (balanceAmount < 2) { // min to play is $2
            int reply = JOptionPane.showConfirmDialog(null, "You don't have enough money left to bet. Would you like to play " +
                    "again?", "Reset Game?", JOptionPane.YES_NO_OPTION);
            if (reply == JOptionPane.YES_OPTION) {
                balanceAmount = 1000;
                updateBalance();
                betAmount = 0;
                updateBet();
                playerPanel.removeAll();
                dealerPanel.removeAll();
                message.setText("");
                roundInProgress(false);
            } else {
                JOptionPane.showMessageDialog(null, "Thanks for playing Blackjack!");
                System.exit(0);
            }
        }
    }
}
