package _07blackjack.blackjack;

import java.util.ArrayList;

/**
 * Creates a player in a blackjack game.
 */
public class Player {
    private String name;
    private ArrayList<Card> hand = new ArrayList<>(); //  the cards that are currently in the player's hand

    public Player(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    // adds a card to the player's hand
    public void addCard(Card card) {
        hand.add(card);
    }

    // returns the blackjack value of the cards in player's hand
    public int getValue() {
        int nValue = getRawValue();
        if (nValue + 10 <= 21 && hasAce()) { // hand is soft
            return nValue + 10;
        } else {
            return nValue;
        }
    }

    // returns true if value of hand is over limit, false otherwise
    public boolean isBusted() {
        return getValue() > Game.LIMIT;
    }

    // returns true if hand has soft 17
    public boolean soft17() {
        return (getRawValue() == 7 && hasAce()); // hand can either be counted as 7 or 17
    }

    // removes all the cards from the player's hand
    public void discardHand() {
        hand = new ArrayList<>();
    }

    // return filenames of cards as an array
    public String[] getCards() {
        String[] cards = new String[hand.size()];
        for (int i = 0; i < cards.length; i++) {
            cards[i] = hand.get(i).toString();
        }
        return cards;
    }

    // returns true if there is an ace in hand
    private boolean hasAce() {
        boolean hasAce = false;
        for (Card card : hand) {
            if (card.isAce()) {
                hasAce = true;
            }
        }
        return hasAce;
    }

    // returns the value of the hand where all aces are counted as 1
    private int getRawValue() {
        int nValue = 0;
        for (Card card : hand) {
            nValue += card.getRank();
        }
        return nValue;
    }
}
