package _07blackjack.blackjack;

/**
 * Creates a card that has rank and suit info.
 */

public class Card {
    private int suit; // from 0 to 3, representing spades, hearts, diamonds, clubs respectively
    private int rank; // from 0 to 12, representing ace, 2, 3, 4, ... jack, queen, king
    private String frontImage; // filename of image to display card in game

    public Card(int suit, int rank, String frontImage) {
        this.suit = suit;
        this.rank = rank;
        this.frontImage = frontImage;
    }

    // returns true if card is an ace
    public boolean isAce() {
        return this.rank == 0;
    }

    // returns the blackjack rank of the card
    public int getRank() {
        if (rank == 0) {
            return 1; // default rank for ace is 1
        } else if (rank >= 9) {
            return 10; // 10, j, q, k
        } else {
            return rank + 1;
        }
    }

    // returns the filename for the image of the card
    @Override
    public String toString() {
        return this.frontImage;
    }
}
