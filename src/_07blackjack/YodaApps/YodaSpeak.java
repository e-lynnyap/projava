package _07blackjack.YodaApps;

import java.util.Scanner;

/**
 * Created by elynn on 11/6/16.
 */
public class YodaSpeak {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a sentence.");
        String strInput = scan.nextLine();
        String[] words = strInput.split(" ");
        System.out.println(reverse(words));
    }

    private static String reverse(String[] words) {
        StringBuilder sb = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) { // append words in backwards order
            sb.append(words[i] + " ");
        }
        return sb.toString();
    }
}
