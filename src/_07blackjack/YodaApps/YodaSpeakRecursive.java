package _07blackjack.YodaApps;

import java.util.Scanner;

/**
 * Created by elynn on 11/6/16.
 */
public class YodaSpeakRecursive {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a sentence.");
        String strInput = scan.nextLine();
        String[] words = strInput.split(" ");
        System.out.println(reverse(words));
    }

    private static String reverse(String[] words) {
        int nLength = words.length;
        return reverse(words, nLength-1);
    }

    private static String reverse(String[] words, int nLastIndex) { // recursive helper method
        if (nLastIndex == 0) { // base case: array has only one word
            return words[0];
        } else {
            return words[nLastIndex] += " " + reverse(words, nLastIndex - 1); // concatenate last word with sub-solution
        }
    }
}
