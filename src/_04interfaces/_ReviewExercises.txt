#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R9.1 Superclass and subclass
a/ Superclass: Employee, Subclass: Manager.
b/ Superclass: Student, Subclass: GraduateStudent
c/ Superclass: Person, Subclass: Student
d/ Superclass: Employee, Subclass: Professor
e/ Superclass: BankAccount, Subclass: CheckingAccount
f/ Superclass: Vehicle, Subclass: Car
g/ Superclass: Vehicle, Subclass: Minivan
h/ Superclass: Car, Subclass: Minivan
i/ Superclass: Vehicle, Subclass: Truck

R9.2 superclass and subclass
In an inventory management system, there would be no need to have data or behavior that is unique to items of a toaster
type vs. items of a travel iron type, etc. The data that each object should have would not vary from one type of
appliance to another, so we could just declare instance variables for quantity, price, etc in the single class
 SmallAppliance; likewise, for behavior, we would invoke the same methods on every object, such as updateQuantity,
 markAsOutOfStock, etc, so this common set of methods could just be defined in the SmallAppliance class. Then, each
 set of appliances would just be instantiated from the SmallAppliance class and updated accordingly, without the need
 for separate subclasses.

 We would only want to have a parent class SmallAppliance and subclasses Toaster, CarVacuum, etc in a situation where
 we want the different types of appliances to share a common set of data and behavior, but expand upon / refine that
 data and behavior in unique ways within each subclass. For example, if we were building a program that simulated the
 uses of the different appliances, then subclassing might be helpful. For example, SmallAppliance would contain common
 methods like switchOn, switchOff, shortCircuit, etc, while Toaster would have additional methods (not shared by other
 subclasses of SmallAppliance) like toastBread, etc. However, in this specific case of inventory management, there is
 no obvious need for differing data and behavior between the types of appliances.

R9.4 SavingsAccount
a/ SavingsAccount inherits the methods deposit and getBalance from BankAccount.
b/ SavingsAccount overrides the monthEnd method and the withdraw method.
c/ SavingsAccount adds the method setInterestRate.

R9.6 Sandwich
a and d are legal, because a superclass variable is allowed to reference a subclass object, though it should be noted
that only the methods defined in Sandwich can be invoked on x.
b and c are not legal, because we are not allowed to assign a superclass object to a subclass reference (unless we cast
the superclass object explicitly).

R9.7 Inheritance -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
See _04interfaces/R9_7.uml (or _04interfaces/R9_7_uml_diagram.png).

R9.8 Inheritance -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
See _04interfaces/R9_8.uml (or _04interfaces/R9_8_uml_diagram.png). Note that the following definitions/responsibilities
have been used to organize the classes: a vehicle is any wheeled object used for transportation purposes, hence
motorcycles and bicycles are types of vehicles. A car is primarily responsible for transporting passengers, while a
truck is primarily responsible for transporting cargo.

R9.9 Inheritance -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located
See _04interfaces/R9_9.uml (or _04interfaces/R9_9_uml_diagram.png). Note that SeminarSpeaker is established as a
separate class from Employee as seminar speakers are generally outside professors who have been invited to speak, so a
seminar speaker is not considered an employee. DepartmentChair is a subclass of Professor because department chairs are
always chosen from professors in the department. TeachingAssistant, Secretary and Janitor are other types of Employees,
and Students are neither Employees or SeminarSpeakers, but they are Persons. A separate hierarchy is established for
Course and its subclasses Seminar, Lecture, and ComputerLab, because these objects do not share common responsibilities
with the Person class and its subclasses.

R9.10 Casting
The difference lies in the risk involved in casting for objects vs. casting for numbers. When we cast from an instance
of a class to an instance of another class, we risk throwing an exception; when we cast from one number to another, we
risk losing precision (e.g. the fractional part of a number that has been cast to an int). Additionally, when we cast
objects, we are only allowed to cast between two objects that are related by inheritance, otherwise the program will
not compile.

R9.11 instanceof operator
The conditions that return true are a, b, d, e. System.out is an object of the PrintStream class (so a is true);
PrintStream is a subclass of FilterOutputStream which is in turn a subclass of OutputStream (so b is true); OutputStream
is a subclass of Object (so d is true), and PrintStream implements Closeable (so e is true).

R9.14 Edible interface
Only the statements a and c are legal. Because the Sandwich class implements the Edible interface, we can assign a
Sandwich object to an Edible reference  (so a is allowed), and we can cast an Edible reference to the Sandwich class and
assign it to a Sandwich reference (so c is allowed). b, d, e, g, h all cause compile-time errors while f causes a
run-time error.


