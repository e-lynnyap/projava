package _04interfaces.P9_10;

import java.awt.*;

/**
 * Created by elynn on 10/22/16.
 */
public class BetterRectangle extends Rectangle {

    // constructor calls the setLocation and setSize methods of Rectangle
    public BetterRectangle(int nX, int nY, int nWidth, int nHeight){
        setLocation(nX, nY);
        setSize(nWidth, nHeight);
    }

    // getPerimeter calculates the perimeter of the rectangle.
    // if rectangle is a line, method returns line height/width
    public double getPerimeter() {
        validateDimensions();
        if (getHeight() == 0) {
            return getWidth(); // rectangle is simply a horizontal line
        } else if (getWidth() == 0) {
            return getHeight(); // rectangle is a vertical line
        } else {
            return 2 * getWidth() + 2 * getHeight();
        }
    }

    // getArea calculates the area of the rectangle
    public double getArea() {
        validateDimensions();
        return getWidth() * getHeight();
    }

    // helper method throws an error if user attempts to perform calculation with invalid dimensions
    private void validateDimensions() {
        if (getHeight() < 0 || getHeight() < 0) {
            throw new IllegalArgumentException("Cannot perform calculation for negative dimensions.");
        }
    }
}
