package _04interfaces.P9_10;

/**
 * Created by elynn on 10/22/16.
 */
public class Driver {
    public static void main(String[] args) {
        // hard coded values for height and width
        int nHeight = 20;
        int nWidth = 30;
        int nHeightLine = 0;
        int nWidthLine = 40;
        int nHeightInvalid = -20;
        int nWidthInvalid = -25;

        // create a BetterRectangle object
        System.out.println("Creating a new rectangle: ");
        BetterRectangle betRect = new BetterRectangle(0, 0, nWidth, nHeight);
        System.out.println("Height of rectangle is: " + betRect.getHeight());
        System.out.println("Width of rectangle is: " + betRect.getWidth());

        // call BetterRectangle methods
        System.out.println();
        System.out.println("Calling BetterRectangle methods:");
        System.out.println("Perimeter of rectangle is: " + betRect.getPerimeter());
        System.out.println("Area of rectangle is: " + betRect.getArea());

        System.out.println();

        // create a BetterRectangle object with height = 0
        System.out.println("Creating a second new rectangle: ");
        BetterRectangle betLine = new BetterRectangle(0, 0, nWidthLine, nHeightLine);
        System.out.println("Height of rectangle is: " + betLine.getHeight());
        System.out.println("Width of rectangle is: " + betLine.getWidth());

        // call BetterRectangle methods
        System.out.println();
        System.out.println("Calling BetterRectangle methods:");
        System.out.print("Perimeter of rectangle is: " + betLine.getPerimeter());
        System.out.println(" (getPerimeter returns height/width if object is a line)");
        System.out.println("Area of rectangle is: " + betLine.getArea());

        System.out.println();

        // create a BetterRectangle object with negative dimensions
        System.out.println("Creating a third new rectangle: ");
        BetterRectangle betInvalid = new BetterRectangle(0, 0, nWidthInvalid, nHeightInvalid);
        System.out.println("Height of rectangle is: " + betInvalid.getHeight());
        System.out.println("Width of rectangle is: " + betInvalid.getWidth());

        // call BetterRectangle methods
        System.out.println();
        System.out.println("Calling BetterRectangle methods:");
        try {
            System.out.println("Perimeter of rectangle is: " + betInvalid.getPerimeter());
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot calculate perimeter for negative dimensions.");
        }
        try {
            System.out.println("Area of rectangle is: " + betInvalid.getArea());
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot calculate area for negative dimensions.");
        }
    }
}
