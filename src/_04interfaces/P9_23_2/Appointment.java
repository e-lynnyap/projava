package _04interfaces.P9_23_2;

import java.util.GregorianCalendar;

/**
 * Created by elynn on 10/31/16.
 */
public class Appointment {
    private String desc;
    private GregorianCalendar date;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public GregorianCalendar getDate() {
        return date;
    }

    public void setDate(GregorianCalendar date) {
        this.date = date;
    }

    public Appointment(String desc, GregorianCalendar date) {

        this.desc = desc;
        this.date = date;
    }

    private boolean occursOn() {
        return false;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "desc='" + desc + '\'' +
                ", date=" + date.getTimeInMillis() +
                '}';
    }
}
