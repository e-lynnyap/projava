package _04interfaces.P9_23_2;

import java.util.GregorianCalendar;

/**
 * Created by elynn on 10/31/16.
 */
public class Monthly extends Appointment {
    public Monthly(String desc, GregorianCalendar date) {
        super(desc, date);
    }
}
