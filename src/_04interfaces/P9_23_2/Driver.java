package _04interfaces.P9_23_2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

/**
 * Created by elynn on 10/31/16. In-class programming example
 */
public class Driver {
    public static void main(String[] args) {

        // program to an interface b/c then we can change ArrayList to any other class that implements
        // the interface (e.g. LinkedList) in the future
        List<Appointment> appointments = new ArrayList<>();

        appointments.add(new Daily("Wake up", new GregorianCalendar(1999, 11, 31)));
        appointments.add(new Onetime("Dentist", new GregorianCalendar(2000, 12, 21)));
        appointments.add(new Onetime("Meet Bob", new GregorianCalendar(2001, 2, 20)));
        appointments.add(new Monthly("Doctor", new GregorianCalendar(2000, 3, 11)));

        Scanner scanner = new Scanner(System.in);

        GregorianCalendar gregorianCalendar;

        // indefinitely until quit
        // ask the user to add an appointment
        // if A
        // add to appointments
        // display all appts
        // if quit
        // display all appts and quit

        outer:
        while (true) {
            System.out.println("Press 'A' to add appointment or press 'Q' to quit.");
            String input = scanner.nextLine();

            switch (input.toUpperCase()) {
                case "A":
                    System.out.println("What is your appointment in the following format Daily, Monthly or Onetime: " +
                            "e.g. Monthly|Pay rent|2016/01/01");
                    String rawAppointment = scanner.nextLine();
                    try {
                        appointments.add(convertStringToAppointment(rawAppointment)); // need to handle IOException potentially thrown by method
                        reportAllAppointments(appointments);
                    } catch (IOException e) {
                        System.out.println(e.getMessage() + ". Try again.");
                        continue;
                    }
                    break;
                case "Q":
                    // report all appts
                    reportAllAppointments(appointments);
                    break outer;
                default:
                    System.out.println("Sorry, please type 'A' or 'Q'.");
                    continue outer;
            }
        }
    }

    private static void reportAllAppointments(List<Appointment> appointments) {
        System.out.println("All appointments: ");
        for (Appointment appointment : appointments) {
            System.out.println(appointment);
        }
    }

    // convert raw string to appointment
    private static Appointment convertStringToAppointment(String raw) throws IOException {
        String[] splits = raw.split("\\|");
        checkNumberParams(splits);

        String type, desc, date;

        type = splits[0];
        desc = splits[1];
        date = splits[2];

        switch (type) {
            case "Daily":
                return new Daily(desc, getGreg(date));
            case "Onetime":
                return new Onetime(desc, getGreg(date));
            case "Monthly":
                return new Monthly(desc, getGreg(date));
            default:
                throw new IOException("That's not an Appoinment type.");
        }
    }

    private static void checkNumberParams(String[] splits) throws IOException {
        if (splits.length != 3) {
            throw new IOException("Too many or too few delimiters.");
        }
    }

    private static GregorianCalendar getGreg(String raw) throws IOException { // anyone who calls this method must deal with possible thrown IOException
        String[] splits = raw.split("/");
        int day, month, year;

        checkNumberParams(splits);

        try {
            day = Integer.parseInt(splits[2]);
            month = Integer.parseInt(splits[1]);
            year = Integer.parseInt(splits[0]);
        } catch (NumberFormatException e) {
            throw new IOException("One of your values is not a number.");
        }

        GregorianCalendar gregorianCalendar;

        try {
            gregorianCalendar = new GregorianCalendar(year, month - 1, day);
        } catch (Exception e) {
            throw new IOException("Your date is out of range.");
        }

        return gregorianCalendar;
    }
}
