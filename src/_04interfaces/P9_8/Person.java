package _04interfaces.P9_8;

/**
 * Created by elynn on 10/22/16.
 */
public class Person {
    // instance variables for name and year of birth
    private String mName;
    private int mYearOfBirth;

    // constructor
    public Person(String strName, int nYear) {
        mName = strName;
        mYearOfBirth = nYear;
    }

    // toString method prints the name of the class and its state
    public String toString() {
        return getClass().getName() + "[name-" + mName +"]" + "[yearOfBirth-" + mYearOfBirth + "]";
    }

}
