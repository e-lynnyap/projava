package _04interfaces.P9_8;

/**
 * Created by elynn on 10/22/16.
 */
public class Instructor extends Person {

    // instance variable for salary, assume integer values only
    private int mSalary;

    // constructor calls the parent class constructor and initializes mMajor
    public Instructor(String strName, int nYear, int nSalary) {
        super(strName, nYear);
        mSalary = nSalary;
    }

    // toString method overrides parent class definition and adds values of subclass instance variables
    public String toString() {
        // cannot access instance variables of superclass directly
        return super.toString() + "[salary-" + mSalary + "]";
    }
}
