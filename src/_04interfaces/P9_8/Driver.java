package _04interfaces.P9_8;

/**
 * Created by elynn on 10/22/16.
 */
public class Driver {
    public static void main(String[] args) {
        // hard coded values for Person, Student, and Instructor objects
        String strPerson = "Bob Dylan";
        int nPersonYear = 1941;
        String strStudent = "Bob";
        int nStudentYear = 1998;
        String strStuMajor = "Music";
        String strInstructor = "Dylan";
        int nInstructorYear = 1958;
        int nInstructorSalary = 80000;

        // create a Person object and call to string method
        System.out.println("Creating a Person object and invoking toString: ");
        Person person = new Person(strPerson, nPersonYear);
        System.out.println(person.toString());

        // create a Student object and call to string method
        System.out.println("Creating a Student object and invoking toString: ");
        Student student = new Student(strStudent, nStudentYear, strStuMajor);
        System.out.println(student.toString());

        // create an Instructor object and call to string method
        System.out.println("Creating an Instructor object and invoking toString: ");
        Instructor instructor = new Instructor(strInstructor, nInstructorYear, nInstructorSalary);
        System.out.println(instructor.toString());
    }
}
