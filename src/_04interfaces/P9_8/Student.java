package _04interfaces.P9_8;

/**
 * Created by elynn on 10/22/16.
 */
public class Student extends Person {

    // instance variable for major
    private String mMajor;

    // constructor calls the parent class constructor and initializes mMajor
    public Student(String strName, int nYear, String strMajor) {
        super(strName, nYear);
        mMajor = strMajor;
    }

    // toString method overrides parent class definition and adds values of subclass instance variables
    public String toString() {
        // cannot access instance variables of superclass directly
        return super.toString() + "[major-" + mMajor + "]";
    }


}
