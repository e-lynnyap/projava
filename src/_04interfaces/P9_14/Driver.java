package _04interfaces.P9_14;

/**
 * Created by elynn on 10/22/16.
 */
public class Driver {
    public static void main(String[] args) {
        // hard code values for radius and height of three SodaCan objects
        double dRadius1 = 3;
        double dHeight1 = 6;
        double dRadius2 = 5.5;
        double dHeight2 = 7;
        double dRadius3 = 2;
        double dHeight3 = 9;

        // create SodaCan objects with hard-coded values
        SodaCan sodCan1 = new SodaCan(dHeight1, dRadius1);
        SodaCan sodCan2 = new SodaCan(dHeight2, dRadius2);
        SodaCan sodCan3 = new SodaCan(dHeight3, dRadius3);

        // print properties
        System.out.println("The first SodaCan object has the following attributes:");
        System.out.print("Height = " + dHeight1 + ", radius = " + dRadius1 + ", surface area (2 dp) = ");
        System.out.printf("%.2f.\n", sodCan1.getSurfaceArea());
        System.out.println();
        System.out.println("The second SodaCan object has the following attributes:");
        System.out.print("Height = " + dHeight2 + ", radius = " + dRadius2 + ", surface area (2 dp) = ");
        System.out.printf("%.2f.\n", sodCan2.getSurfaceArea());
        System.out.println();
        System.out.println("The third SodaCan object has the following attributes:");
        System.out.print("Height = " + dHeight3 + ", radius = " + dRadius3 + ", surface area (2 dp) = ");
        System.out.printf("%.2f.\n", sodCan3.getSurfaceArea());
        System.out.println();

        // print average surface area of soda cans
        Measurable[] sodCans = new Measurable[]{sodCan1, sodCan2, sodCan3};
        System.out.println("The average surface area of the three soda cans (rounded to 2 dp) is: ");
        System.out.printf("%.2f.\n", average(sodCans));

    }

    // universal method for computing averages which works for any object implementing Measurable
    public static double average(Measurable[] objects) {
        if (objects.length == 0) {
            return 0;
        }
        double sum = 0;
        for (Measurable obj : objects) {
            sum += obj.getMeasure();
        }
        return sum / objects.length;
    }
}
