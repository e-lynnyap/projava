package _04interfaces.P9_14;

/**
 * Created by elynn on 10/22/16.
 */
public class SodaCan implements Measurable {

    // declare height and radius as private instance variables
    private double mHeight;
    private double mRadius;

    // constructor takes in height and radius as arguments
    public SodaCan(double dHeight, double dRadius) {
        // throw an error if user tries to construct object with negative values
        if (!positiveDouble(dHeight) || !positiveDouble(dRadius)) {
            throw new IllegalArgumentException("Arguments must be positive numbers");
        }
        mHeight = dHeight;
        mRadius = dRadius;
    }

    // public method to return surface area
    public double getSurfaceArea() {
        return 2 * Math.PI * mRadius * mHeight + 2 * Math.PI * mRadius * mRadius;
    }

    // implementation of getMeasure method in Measurable interface
    public double getMeasure() {
        return getSurfaceArea();
    }

    // private method to validate numbers
    private Boolean positiveDouble(double dValue) {
        return (dValue > 0);
    }
}
