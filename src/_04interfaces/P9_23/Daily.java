package _04interfaces.P9_23;

/**
 * Created by elynn on 10/23/16.
 */
public class Daily extends Appointment {

    public Daily(String strDesc, int nYear, int nMonth, int nDay) {
        super(strDesc, nYear, nMonth, nDay);
    }

    @Override
    // override occursOn method from parent class; return true if date is on or after the appointment date
    public boolean occursOn(int nYear, int nMonth, int nDay) {
        if (nYear > getYear()) {
            return true; // if date occurs in a later year
        } else if (nYear < getYear()) {
            return false; // if date occurs in a prior year
        } else {
            if (nMonth > getMonth()) {
                return true; // if date occurs in same year, but later month
            } else if (nMonth < getMonth()) {
                return false; // if date occurs in same year, but prior month
            } else {
                return (nDay >= getDay()); // if date occurs in same year and month, but later/on date
            }
        }
    }
}
