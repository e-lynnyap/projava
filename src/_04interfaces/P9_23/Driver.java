package _04interfaces.P9_23;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * Created by elynn on 10/23/16.
 */
public class Driver {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        ArrayList<Appointment> appointments = new ArrayList<>();
        int nAction;
        boolean bRunProgram = true; // set to false and exit loop if user chooses to quit

        while (bRunProgram) {

            // prompt user for action that they want to take
            nAction = determineAction(scan);

            // execute action based on user input
            // 1: create appointments
            if (nAction == 1) {

                // get input from user to add Appointment objects to arraylist
                System.out.println("You may enter information about the appointments you want to create:");
                addAppointments(appointments, scan);

                System.out.println();
                printAppointments(appointments);

                System.out.println("----------------------------------------");

            } else if (nAction == 2) { // 2: check date for appointments

                // read input from user to print all appointments that occur on date
                System.out.println("You may enter a date to check what appointments occur on this date:");
                checkAppointments(appointments, scan);

                System.out.println("----------------------------------------");

            } else if (nAction == 3) { // 3: save appointments to file

                // save the appointments to a file
                System.out.println("We will now save the appointments created so far into a file.");
                save(appointments, scan);

                System.out.println("----------------------------------------");

            } else if (nAction == 4) { // 4: load appointments from file

                // load the appointments from a file
                System.out.println("We will now create appointments by loading them from a file.");
                loadFromFile(appointments, scan);

                // print out the appointments that were loaded in
                System.out.println();
                printAppointments(appointments);
                System.out.println("----------------------------------------");

            } else if (nAction == 5) { // 5: exit program
                bRunProgram = false;
            }
        }
        System.out.println("Thanks for using Appointment Book.");

    }

    // method to ask user what action they want to take
    public static int determineAction(Scanner scan) {
        int nAction;
        System.out.println("Please choose what you would like to do: ");
        System.out.println("(1) Create new appointments.");
        System.out.println("(2) Find out which appointments have been scheduled for a particular date.");
        System.out.println("(3) Save all appointments to a file.");
        System.out.println("(4) Load new appointments from a file.");
        System.out.println("(5) Exit program.");
        do {
            System.out.println("Please enter 1, 2, 3, 4, or 5.");
            validateInt(scan);
            nAction = scan.nextInt();
        } while (nAction < 1 || nAction > 5);
        System.out.println();
        return nAction;
    }

    // method to ask user what appointments they want to add
    public static void addAppointments(ArrayList<Appointment> appointments, Scanner scan) {
        int nAppCount;
        int nYear;
        int nMonth;
        int nDay;
        String strDesc;
        int nType;

        System.out.println("How many appointments would you like to create?");
        do {
            System.out.println("Please enter a positive number.");
            validateInt(scan);
            nAppCount = scan.nextInt();
        } while (nAppCount <= 0);

        for (int i = 0; i < nAppCount; i++) {

            // get year
            System.out.println("Enter the year of Appointment #" + (i + 1) + ":");
            nYear = readYear(scan);

            // get month
            System.out.println("Enter the month of Appointment #" + (i + 1) + ":");
            nMonth = readMonth(scan);

            // get day
            System.out.println("Enter the day of Appointment #" + (i + 1) + ":");
            nDay = readDay(scan);

            scan.nextLine(); // consume newline character

            // get description
            System.out.println("Enter the description of Appointment " + (i + 1) + ":");
            strDesc = scan.nextLine();

            // get appointment type
            System.out.println("Is this appointment (1) Onetime, (2) Daily, or (3) Monthly?");
            do {
                System.out.println("Please enter 1, 2, or 3.");
                validateInt(scan);
                nType = scan.nextInt();
            } while (nType < 1 || nType > 3);

            // add appointments to array
            if (nType == 1) {
                appointments.add(new Onetime(strDesc, nYear, nMonth, nDay));
            } else if (nType == 2) {
                appointments.add(new Daily(strDesc, nYear, nMonth, nDay));
            } else if (nType == 3) {
                appointments.add(new Monthly(strDesc, nYear, nMonth, nDay));
            }
        }
    }

    // method to check what appointments occur on a given date
    public static void checkAppointments(ArrayList<Appointment> appointments, Scanner scan) {
        int nYear;
        int nMonth;
        int nDay;

        System.out.println("Enter a date to see what appointments are scheduled to occur.");
        System.out.println("First, enter the year: ");
        nYear = readYear(scan);
        System.out.println("Next, enter the month.");
        nMonth = readMonth(scan);
        System.out.println("Finally, enter the day.");
        nDay = readDay(scan);

        int nNumOfApps = 0;
        ArrayList<String> strAppointments = new ArrayList<>();

        // find all appointments scheduled to occur on date
        for (Appointment app : appointments) {
            if (app.occursOn(nYear, nMonth, nDay)) {
                nNumOfApps += 1;
                strAppointments.add(app.toString());
            }
        }

        // print out results
        System.out.println("The number of appointments scheduled for " + nMonth + "/" + nDay + "/" + nYear
                + " is: " + nNumOfApps);
        for (String app : strAppointments) {
            System.out.println(app);
        }
    }

    // method to save array of appointments to file
    public static void save(ArrayList<Appointment> appointments, Scanner scan) {
        // ask user for filename that appointments should be saved to
        String strFileName = readFileName(scan);

        // create a file and write appointments to file
        PrintWriter output;
        try {
            output = new PrintWriter(strFileName);
            for (Appointment app : appointments) {
                output.print(app.getType() + " " + app.getYear() + " " + app.getMonth() + " " + app.getDay());
                output.println(" " + app.getDescription());
            }
            output.close();
            System.out.println(strFileName + " saved successfully.");
        } catch (FileNotFoundException e) {
            System.out.println("Error writing file.");
        }
    }

    // method to load appointments from a file
    public static void loadFromFile(ArrayList<Appointment> appointments, Scanner scan) {

        // ask user for name of file where appointments will be loaded from
        String strFileName = readFileName(scan);

        try {
            Scanner file = new Scanner(new File(strFileName));

            String strType;
            int nYear;
            int nMonth;
            int nDay;
            String strDesc;

            // read appointments from file into arraylist
            while (file.hasNext()) {
                strType = file.next();
                nYear = file.nextInt();
                nMonth = file.nextInt();
                nDay = file.nextInt();
                strDesc = file.nextLine().trim();
                appointments.add(createAppointment(strType, nYear, nMonth, nDay, strDesc));
            }
            System.out.println("Appointments loaded successfully.");
        } catch (FileNotFoundException e) {
            System.out.println("File was not found.");
        }
    }

    // method to create an appointment of the appropriate type
    public static Appointment createAppointment(String strType, int nYear, int nMonth, int nDay, String strDesc) {
        if (strType.equals("Onetime")) {
            return new Onetime(strDesc, nYear, nMonth, nDay);
        } else if (strType.equals("Daily")) {
            return new Daily(strDesc, nYear, nMonth, nDay);
        } else if (strType.equals("Monthly")) {
            return new Monthly(strDesc, nYear, nMonth, nDay);
        } else {
            throw new IllegalArgumentException("Invalid appointment type.");
        }
    }

    // method to print out all appointments so far
    public static void printAppointments(ArrayList<Appointment> appointments) {
        System.out.println("Your current list of appointments is: ");
        for (Appointment app : appointments) {
            System.out.println(app.toString());
        }
    }

    // method to read filename
    public static String readFileName(Scanner scan) {
        // prompt user for filename
        System.out.println("Enter a filename (include the extension):");
        String strFileName;
        do {
            System.out.println("Please enter a non-empty string for the filename.");
            strFileName = scan.next().trim();
        } while (strFileName.length() == 0);
        return strFileName;
    }

    // method to validate integer value being entered
    public static void validateInt(Scanner scan) {
        while (!scan.hasNextInt()) {
            System.out.println("You must enter an integer value.");
            scan.next();
        }
    }

    // method to read valid year from user input
    public static int readYear(Scanner scan) {
        int nYear;
        do {
            System.out.println("Please enter a positive number.");
            validateInt(scan);
            nYear = scan.nextInt();
        }  while (nYear <= 0);
        return nYear;
    }

    // method to read valid month from user input
    public static int readMonth(Scanner scan) {
        int nMonth;
        do {
            System.out.println("Please enter a number between 1-12.");
            validateInt(scan);
            nMonth = scan.nextInt();
        }  while (nMonth < 1 || nMonth > 12);
        return nMonth;
    }

    public static int readDay(Scanner scan) {
        int nDay;
        do {
            System.out.println("Please enter a number between 1-31.");
            validateInt(scan);
            nDay = scan.nextInt();
        }  while (nDay < 1 || nDay > 31);
        return nDay;
    }
}
