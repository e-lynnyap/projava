package _04interfaces.P9_23;

/**
 * Created by elynn on 10/23/16.
 */
public class Monthly extends Appointment {

    public Monthly(String strDesc, int nYear, int nMonth, int nDay) {
        super(strDesc, nYear, nMonth, nDay);
    }

    @Override
    // override occursOn method from parent class; returns true if day matches and month is on or greater
    public boolean occursOn(int nYear, int nMonth, int nDay) {
        if (nDay != getDay()) {
            return false; // day does not match
        } else {
            if (nYear > getYear()) {
                return true; // occurs in a later year
            } else if (nYear < getYear()) {
                return false; // occurs in a prior year
            } else {
                return (nMonth >= getMonth()); // occurs in same year, and month is equal or greater
            }
        }
    }
}
