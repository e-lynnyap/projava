package _04interfaces.P9_23;

/**
 * Created by elynn on 10/23/16.
 */
public class Onetime extends Appointment {

    public Onetime(String strDesc, int nYear, int nMonth, int nDay) {
        super(strDesc, nYear, nMonth, nDay);
    }
}
