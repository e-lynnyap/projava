package _04interfaces.P9_23;

/**
 * Created by elynn on 10/23/16.
 */
public class Appointment {

    // instance variables for description, year, month, day
    private String mDesc;
    private int mYear;
    private int mMonth;
    private int mDay;

    // constructor takes in year, month, day, description as arguments
    public Appointment(String strDesc, int nYear, int nMonth, int nDay) {
        if (nYear <= 0 || nMonth <= 0 || nDay <= 0) {
            throw new IllegalArgumentException("Dates must be positive numbers.");
        }
        if (nMonth < 1 || nMonth > 12) {
            throw new IllegalArgumentException("Month must be between 1-12.");
        }
        if (nDay < 1 || nDay > 31) {
            throw new IllegalArgumentException("Day must be between 1-31.");
        }
        mDesc = strDesc;
        mYear = nYear;
        mMonth = nMonth;
        mDay = nDay;
    }

    // occursOn method returns true if the appointment occurs on the exact date supplied
    public boolean occursOn(int nYear, int nMonth, int nDay) {
        return (mYear == nYear) && (mMonth == nMonth) && (mDay == nDay);
    }

    // getType returns appointment type
    public String getType() {
        if (this instanceof Onetime) {
            return "Onetime";
        } else if (this instanceof Daily) {
            return "Daily";
        } else if (this instanceof Monthly) {
            return "Monthly";
        } else {
            return "Unknown";
        }
    }

    // getter methods to retrieve fields
    public String getDescription() {
        return mDesc;
    }

    public int getYear() {
        return mYear;
    }

    public int getMonth() {
        return mMonth;
    }

    public int getDay() {
        return mDay;
    }

    // toString method describes object
    public String toString() {
        return getClass().getName() + "[year-" + mYear + "][month-" + mMonth + "][day-" + mDay + "][description-" + mDesc + "]";
    }
}
