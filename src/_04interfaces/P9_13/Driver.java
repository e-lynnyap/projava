package _04interfaces.P9_13;

/**
 * Created by elynn on 10/22/16.
 */
public class Driver {
    public static void main(String[] args) {
        // hard coded values for labeled point coordinates and label
        int nX = 20;
        int nY = 100;
        String strLabel = "New labeled point";

        // create a LabeledPoint object
        System.out.println("Creating a new LabeledPoint object with the following attributes: ");
        LabeledPoint labPoint = new LabeledPoint(nX, nY, strLabel);
        System.out.println("x-coordinate: " + nX);
        System.out.println("y-coordinate: " + nY);
        System.out.println("Label: " + strLabel);

        System.out.println();

        // invoke toString method on object
        System.out.println("Invoking the toString method on object:");
        System.out.println(labPoint.toString());
    }
}
