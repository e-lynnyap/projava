package _04interfaces.P9_13;

import java.awt.*;
/**
 * Created by elynn on 10/22/16.
 */
public class LabeledPoint {

    // instance variables for point (stores location) and label
    private String mLabel;
    private Point mLocation;

    // constructor stores location in a java.awt.Point object
    public LabeledPoint(int nX, int nY, String strLabel) {
        mLocation = new Point(nX, nY);
        mLabel = strLabel;
    }

    // toString method displays x, y, and the label
    public String toString() {
        return getClass().getName() + "[label=" + mLabel + "]" + "[location=" + mLocation.toString() + "]";
    }
}
