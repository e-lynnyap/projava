package _04interfaces.P9_16;

/**
 * Created by elynn on 10/23/16.
 */

// interface allows methods of statistical analysis to be invoked on objects of implementing classes
public interface Measurable {
    double getMeasure();
}
