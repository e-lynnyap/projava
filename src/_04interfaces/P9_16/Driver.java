package _04interfaces.P9_16;

/**
 * Created by elynn on 10/23/16.
 */

// this program creates an array of Country objects and finds the one with the largest area
public class Driver {
    public static void main(String[] args) {
        // hard coded values for countries
        String strCountry1 = "Russia";
        double dCountry1 = 17075200;
        String strCountry2 = "USA";
        double dCountry2 = 9629091;
        String strCountry3 = "Indonesia";
        double dCountry3 = 1919440;
        String strCountry4 = "China";
        double dCountry4 = 9596960;

        // create Country objects
        Country couRussia = new Country(strCountry1, dCountry1);
        Country couUsa = new Country(strCountry2, dCountry2);
        Country couIndonesia = new Country(strCountry3, dCountry3);
        Country couChina = new Country(strCountry4, dCountry4);

        // print out country properties
        System.out.println("Calculating the maxmimum area out of the following countries: ");
        System.out.println(couRussia.getName() + ": " + couRussia.getMeasure() + " sq km");
        System.out.println(couUsa.getName() + ": " + couUsa.getMeasure() + " sq km");
        System.out.println(couIndonesia.getName() + ": " + couIndonesia.getMeasure() + " sq km");
        System.out.println(couChina.getName() + ": " + couChina.getMeasure() + " sq km");

        // create array of Country objects
        Measurable[] meaCountries = new Measurable[]{couRussia, couUsa, couIndonesia, couChina};

        // calculate the maximum area out of the countries
        System.out.println();
        System.out.println("The country with the largest area is: ");
        Country couMaxArea = (Country) maximum(meaCountries);
        System.out.println(couMaxArea.getName() + ": " + couMaxArea.getMeasure() + " sq km.");
    }

    // maximum method returns the object with the largest measure
    public static Measurable maximum(Measurable[] objects) {
        Measurable meaMax = objects[0];
        for (Measurable obj : objects) {
            if (obj.getMeasure() > meaMax.getMeasure()) {
                meaMax = obj;
            }
        }
        return meaMax;
    }
}
