package _04interfaces.P9_16;

/**
 * Created by elynn on 10/23/16.
 */
public class Country implements Measurable {

    // declare instance variables for name and area
    private String mName;
    private double mArea;

    // constructor takes in name and area (in sq km) as arguments
    public Country(String strName, double dArea) {
        // throw exceptions for invalid arguments
        if (strName == "") {
            throw new IllegalArgumentException("Cannot pass in empty string for Name field.");
        }
        if (dArea <= 0) {
            throw new IllegalArgumentException("Area must be a positive number.");
        }
        mName = strName;
        mArea = dArea;
    }

    // implement getMeasure method from Measurable interface
    public double getMeasure() {
        return mArea;
    }

    // getter method for country name
    public String getName() {
        return mName;
    }

}
