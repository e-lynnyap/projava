package _01control;
import java.util.Scanner;

/**
 * Created by elynn on 9/28/16.
 */
public class P3_26 {
    public static void main(String[] args) {
        //read input
        Scanner scan = new Scanner(System.in);
        int nInput = scan.nextInt();

        // store roman numerals and numeric equivalents in parallel arrays
        String[] strRomanNumerals = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        Integer[] nNumericValues = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

        // convert to roman numerals
        String strOutput = "";
        Integer nIndex = 0;
        while (nInput > 0) {
            // count the number of times each letter appears
            Integer nRomanNumeralCount = nInput / nNumericValues[nIndex];

            // append letters to the final result
            for (int j = 0; j < nRomanNumeralCount; j++){
                strOutput += strRomanNumerals[nIndex];
            }

            // calculate remainder left to compute using the other letters
            nInput %= nNumericValues[nIndex];
            nIndex += 1;
        }

        // print output
        System.out.println(strOutput);
    }
}
