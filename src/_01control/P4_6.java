package _01control;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by elynn on 9/28/16.
 */
public class P4_6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a series of numbers, separated by spaces or new lines.");
        System.out.println("Type in any letter at the end of your input.");
        System.out.println("E.g. type '123 245 59 2305 230 q' and hit enter.");
        Boolean bFirst = true;
        Integer nMinimum = 0;
        while(scan.hasNextInt()) {
            if (bFirst) {
                nMinimum = scan.nextInt();
                bFirst = false;
            } else {
                Integer nValue = scan.nextInt();
                if (nValue < nMinimum) {
                    nMinimum = nValue;
                }
            }
        }
        System.out.println("The smallest value you entered is: " + nMinimum);
    }
}
