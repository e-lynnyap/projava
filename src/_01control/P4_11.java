package _01control;
import java.util.Scanner;

/**
 * Created by elynn on 9/26/16.
 */
public class P4_11 {
    public static void main(String[] args) {
        // read input
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a word: ");
        char[] cLetters = scan.next().toCharArray();

        Integer nSyllableCount = 0;

        // flag will be set to true if vowel is encountered
        Boolean bAdjacentVowel = false;

        // iterate through characters and increase count for each sequence of adjacent vowels
        for (char c : cLetters){
            Boolean bIsConsonant = "aeiouyAEIOUY".indexOf(c) == -1;
            if (bAdjacentVowel) {
                if (bIsConsonant) {
                    bAdjacentVowel = false;
                }
            } else {
                if (!bIsConsonant) {
                    nSyllableCount += 1;
                    bAdjacentVowel = true;
                }
            }
        }

        // adjust count if last character is e
        if (cLetters[cLetters.length - 1] == 'e') {
            nSyllableCount -= 1;
        }

        // adjust count if it is 0
        if (nSyllableCount == 0) {
            nSyllableCount += 1;
        }

        System.out.println("The number of syllables is: " + nSyllableCount);
    }
}
