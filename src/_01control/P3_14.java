package _01control;
import java.util.Scanner;

/**
 * Created by elynn on 9/28/16.
 */
public class P3_14 {
    public static void main(String[] args) {
        // pseudocode
        // print instructions
        // read user input, store in character array named 'cCardDetails'
        // determine value by evaluating first char in cCardDetails
        // determine suit by evaluating second char in cCardDetails
        // print value and suit

        Scanner scan = new Scanner(System.in);
        String strValue = "";
        String strSuit = "";

        System.out.print("Enter the card notation: ");
        char[] cCardDetails = scan.next().toCharArray();

        switch (cCardDetails[0]){
            case 'A':
                strValue = "Ace";
                break;
            case 'J':
                strValue = "Jack";
                break;
            case 'Q':
                strValue = "Queen";
                break;
            case 'K':
                strValue = "King";
                break;
            default:
                strValue = String.valueOf(cCardDetails[0]);
        }

        switch(cCardDetails[1]){
            case 'D':
                strSuit = "Diamonds";
                break;
            case 'H':
                strSuit = "Hearts";
                break;
            case 'S':
                strSuit = "Spades";
                break;
            case 'C':
                strSuit = "Clubs";
                break;
            default:
                break;
        }

        System.out.println(strValue + " of " + strSuit);
    }
}
