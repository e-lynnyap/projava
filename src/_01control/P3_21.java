package _01control;
import java.util.Scanner;
import java.text.DecimalFormat;

/**
 * Created by elynn on 9/28/16.
 */
public class P3_21 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        // read input
        System.out.print("Enter your income: ");
        double dIncome = scan.nextDouble();

        // calculate tax
        double dTaxPercentage;
        if (dIncome < 50000) {
            dTaxPercentage = 0.01;
        } else if (dIncome <= 75000) {
            dTaxPercentage = 0.02;
        } else if (dIncome <= 100000) {
            dTaxPercentage = 0.03;
        } else if (dIncome <= 250000) {
            dTaxPercentage = 0.04;
        } else if (dIncome <= 500000) {
            dTaxPercentage = 0.05;
        } else {
            dTaxPercentage = 0.06;
        }

        // calculate total tax owed
        double dTaxTotal = dIncome * dTaxPercentage;

        // format output
        String strPattern = "$#.00";
        DecimalFormat myFormatter = new DecimalFormat(strPattern);
        String output = myFormatter.format(dTaxTotal);

        // print output
        System.out.println(output);
        System.out.println("Your tax due is " + output);
    }
}
