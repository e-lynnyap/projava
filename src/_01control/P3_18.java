package _01control;
import java.util.Scanner;

/**
 * Created by elynn on 9/28/16.
 */
public class P3_18 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        // get input from user
        System.out.print("Enter the month (1-12): ");
        Integer nMonth = scan.nextInt();
        System.out.print("Enter the day (1-31): ");
        Integer nDay = scan.nextInt();

        // evaluate input
        String strSeason;
        if (nMonth <= 3) {
            strSeason = "Winter";
        } else if (nMonth <= 6) {
            strSeason = "Spring";
        } else if (nMonth <= 9) {
            strSeason = "Summer";
        } else {
            strSeason = "Fall";
        }

        if (nMonth % 3 == 0 && nDay >= 21) {
            if (strSeason.equals("Winter")) {
                strSeason = "Spring";
            } else if (strSeason.equals("Spring")) {
                strSeason = "Summer";
            } else if (strSeason.equals("Summer")) {
                strSeason = "Fall";
            } else {
                strSeason = "Winter";
            }
        }

        System.out.println("The season is " + strSeason);
    }
}
