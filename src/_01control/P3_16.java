package _01control;
import java.util.Scanner;

/**
 * Created by elynn on 9/28/16.
 */
public class P3_16 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] strInputs = new String[3];

        // read input, store in array
        for (int i = 0; i < 3; i++) {
            strInputs[i] = scan.next();
        }

        // sort input
        for (int i = 1; i < 3; i++){
            for (int j = 0; j < i; j++){
                if (strInputs[i].compareTo(strInputs[j]) < 0) {
                    // check all pairs and swap string positions if in reverse alphabetical order
                    String strTemp = strInputs[j];
                    strInputs[j] = strInputs[i];
                    strInputs[i] = strTemp;
                }
            }
        }

        // print strings in order
        for (int i = 0; i < 3; i++){
            System.out.println(strInputs[i]);
        }
    }
}
