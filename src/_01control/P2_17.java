package _01control;
import com.sun.tools.doclets.formats.html.SourceToHTMLConverter;

import java.util.Scanner;

/**
 * Created by elynn on 9/28/16.
 */
public class P2_17 {
    public static void main(String[] args) {
        // pseudocode
        // print instructions
        // read input: first time and second time
        // calculate difference between times
        // hours = difference / 100
        // minutes = difference % 100
        // print hours and minutes

        Scanner scan = new Scanner(System.in);
        int nFirstTime, nSecondTime;

        do {
            System.out.print("Please enter the first time: ");
            nFirstTime = scan.nextInt();
        } while (nFirstTime < 0 || nFirstTime > 2359);

        do {
            System.out.print("Please enter the second time: ");
            nSecondTime = scan.nextInt();
        } while (nSecondTime < 0 || nSecondTime > 2359);

        int nDifference = Math.abs(nSecondTime - nFirstTime);
        int nHours = nDifference / 100;
        int nMinutes = nDifference % 100;

        System.out.println(nHours + " hours " + nMinutes + " minutes");
    }
}
