package _01control;
import java.util.Scanner;

/**
 * Created by elynn on 9/26/16.
 */
public class P4_18 {
    public static void main(String[] args) {
        // read input
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number: ");
        Integer nNumber = scan.nextInt();

        // iterate through all numbers up to the number and print if prime
        for (int i = 2; i <= nNumber; i++) {
            if (isPrime(i)) {
                System.out.println(i);
            }
        }
    }

    public static boolean isPrime(Integer num) {
        if (num == 1) {
            return false;
        }
        // if number is not prime, it will have a factor less than its square root
        for (int i = 2; i <= Math.sqrt(num * 1.0); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
