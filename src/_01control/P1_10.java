package _01control;

/**
 * Created by Adam on 9/28/2015.
 */
public class P1_10 {

    public static void main(String[] args) {
        // Ascii art from http://www.ascii-art.de/ascii/def/fox.txt
        // Poetry from http://www.azlyrics.com/lyrics/ylvis/thefox.html

        System.out.println("   ------------------------------------      /\\   /\\");
        System.out.println(" | Joff-tchoff-tchoffo-tchoffo-tchoff!  |   //\\\\_//\\\\     ____");
        System.out.println(" | Tchoff-tchoff-tchoffo-tchoffo-tchoff!|   \\_     _/    /   /");
        System.out.println(" | Joff-tchoff-tchoffo-tchoffo-tchoff!  |    / * * \\    /^^^]");
        System.out.println(" | !@&#&$%$!%#@#$#&**#@#!#!%#$^^%*%#$%@  >   \\_\\O/_/    [   ]");
        System.out.println("  -------------------------------------       /   \\_    [   /");
        System.out.println("                                              \\     \\_  /  /");
        System.out.println("                                               [ [ /  \\/ _/");
        System.out.println("                                              _[ [ \\  /_/");

    }
}
