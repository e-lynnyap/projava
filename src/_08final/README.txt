Description of Features:

KEY PRESSES

- Teleport: Player can teleport falcon to a random location on screen by pressing 'd'. The text at the top of
the screen shows how many teleports are left to use. If the number is 0, the player cannot teleport. In order to
teleport, the player must collect more TeleportFloaters (see FLOATERS below). See /mvc/controller/Game.java: 685-687
for implementation of teleport functionality. Falcon is re-protected upon teleport (to avoid teleporting straight into
a foe and dying).

- Shield: Player can activate shield around falcon by pressing 'a'. Like teleport, shield uses are limited and can only
be increased by collecting the right floaters. See /mvc/controller/Game.java: 699-710 for implementation of shield
functionality. See /mvc/model/Falcon: 240-245 for implementation of shield visual appearance (two circles around Falcon)

UFO

- New Foe created: see /mvc/model/Ufo.java. There are 3 levels to this UFO. Level 1 UFOs just move from left to right randomly,
level 2 UFOs move in diagonals randomly, level 3 UFOs move towards and away from the Falcon, and also shoot bullets
aimed directly at the Falcon's current position. At each level, UFO decreases in size and increases in health. When a bullet
hits a ufo, its health decreases. If health reaches 0, it spawns a higher-level ufo, or if it is a level 3 ufo, simply
gets destroyed. See /mvc/controller/Game.java: 383-397 for implementation of ufo/Friend collision logic.
See /mvc/model/UfoBullet.java for implementation of bullets for level 3 ufos.

FLOATERS

Whenever falcon collides with floater, a 'bonus' of some sort is added, in addition to an increased score for collecting
the floater. See /mvc/controller/Game.java: 213-239 for implementation of floater logic, and the individual classes for
each floater. All five classes extend the Floater parent class as they share common appearance and movement.

Note: to make the floaters appear more frequently, change the constants in /mvc/controller/Game.java: 62-67 to lower
numbers.

- NewShipFloater (blue): The falcon is cloned into a yellow falcon that moves around and helps to shoot enemies randomly.
Clone is always protected so it doesn't matter if it crashes into a foe.

- TripleBulletFloater (red): When 'space' is pressed, falcon fires three bullet at once instead of one. See
/mvc/controller/Game.java: 644-652 for implementation of triple bullet functionality.

- ShieldFloater (cyan): increases shield count by one.

- TeleportFloater (pink): increases teleport count by one.

- LifeFloater (yellow): increases number of falcon lives (shown at bottom-right of screen) by one.

NewShipFloater and TripleBulletFloater implement the Limitable interface, which means that they only work for a limited
time. The green meter at the bottom left of the screen indicates how much time is left to use the power ups. See
/mvc/controller/Game.java: 328-347 for implementation of time limit functionality, and /mvc/view/GamePanel: 208-218
for implementation of power-up meter visual effect.

SCORING

- Objects of classes that implement Scorable interface will increase the score when they collide with the Falcon. E.g.
Floater and the different Foes.

PORTAL

- When a level is completed (i.e. all UFOs and asteroids destroyed), a portal appears for the falcon to fly through, in
order to advance to the next level. See /mvc/model/Portal for implementation of visual appearance and
/mvc/controller/Game.java: 244-260 + 525-546 for implementation of portal appearance/collision logic.

VISUALS

- Debris class (which extends Sprite) created to produce the effect of the foe's outline exploding into different lines,
radiating away from the center, changing color randomly for a rainbow effect. See /mvc/model/Debris.java and
/mvc/controller/Game.java: 414-430 for implementation.

- Colors have been changed for the Falcon to make it more visible against the background; falcon's bullets also matches
falcon's colors.

- Background is randomly generated at the start of each new level to give the appearance of a different location after
the falcon goes through the portal. See /mvc/controller/Game.java: 559-570 for implementation of this background. A new
Sprite sub-class has been created (Star) to implement the stars in the background.

- Starting screen displays all the relevant rules and there is a legend for the different types of floaters at the top
of the screen that is visible during gameplay.

SOUND EFFECTS

- New sound effects for various actions in the game, e.g. teleporting, shooting three bullets, hitting a ufo, falcon
getting damaged, portal opening, going through portal, activating shield.

FALCON MOVEMENT

- Added 'friction' to the falcon's movement so that it slows down after releasing the up key (so that falcon is generally
easier to control). See /mvc/model/Falcon.java: 16 + 174-175.

Note: this is not part of the game's official functionality, but if you press 'Enter', you'll 'hit' all the foes on
screen - may be helpful when testing the game.