package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;

public class NewShipFloater extends Floater implements Limitable {

	public NewShipFloater() {
		super();
		setColor(Color.BLUE);
	}

	public NewShipFloater(int nRadius, Point point) {
		super(nRadius, point);
		setColor(Color.BLUE);
	}

	@Override
	public int getScore() {
		return 200;
	}

	@Override
	public int getLimit() {
		return 300;
	}
}
