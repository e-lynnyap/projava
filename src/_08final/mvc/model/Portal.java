package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;

/**
 * Created by elynn on 11/30/16.
 */
public class Portal extends Sprite {

    public final int RADIUS = 50;
    private boolean fadeOut = true;

    public Portal() {
        super();
        setTeam(Team.PORTAL);
        setRadius(RADIUS);

        int falconX = (int)CommandCenter.getInstance().getFalcon().getCenter().getX();
        int falconY = (int)CommandCenter.getInstance().getFalcon().getCenter().getY();

        setCenter(new Point((falconX + 300) % ((int)(Game.DIM.getWidth() - RADIUS*2)) + RADIUS ,
                            (falconY + 300) % ((int)(Game.DIM.getHeight())-RADIUS*2) + RADIUS));

        setColor(Color.BLACK);
    }

    @Override
    public void draw(Graphics g) {

        g.setColor(Color.BLACK);
        g.fillOval((int)getCenter().getX()-RADIUS, (int)getCenter().getY()-RADIUS, RADIUS*2, RADIUS*2);

        g.setColor(new Color(Game.R.nextInt(256), Game.R.nextInt(256), Game.R.nextInt(256)));
        g.drawOval((int)getCenter().getX()-RADIUS, (int)getCenter().getY()-RADIUS, RADIUS*2, RADIUS*2);
        g.setColor(new Color(Game.R.nextInt(256), Game.R.nextInt(256), Game.R.nextInt(256)));
        g.drawOval((int)getCenter().getX()-RADIUS*2/3, (int)getCenter().getY()-RADIUS*2/3, RADIUS*4/3, RADIUS*4/3);
        g.setColor(new Color(Game.R.nextInt(256), Game.R.nextInt(256), Game.R.nextInt(256)));
        g.drawOval((int)getCenter().getX()-RADIUS*1/3, (int)getCenter().getY()-RADIUS*1/3, RADIUS*2/3, RADIUS*2/3);

    }

    @Override
    public void move() {

    }

}
