package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by elynn on 11/14/16.
 */
public class Debris extends Sprite implements Movable {
    private int mExpiry;

    int nX1;
    int nY1;
    int nX2;
    int nY2;

    private Point mCenter;

    public Debris(Point mPoint1, Point mPoint2, Point center) {
        super();

        ArrayList<Point> points = new ArrayList<>();
        points.add(mPoint1);
        points.add(mPoint2);
        assignPolarPoints(points);

        nX1 = (int)mPoint1.getX();
        nY1 = (int)mPoint1.getY();
        nX2 = (int)mPoint2.getX();
        nY2 = (int)mPoint2.getY();

        mExpiry = 20;
        mCenter = center;

    }

    // passes in the graphic context of the off-screen graphic
    @Override
    public void draw(Graphics g) {
        g.setColor(new Color(Game.R.nextInt(256), Game.R.nextInt(256), Game.R.nextInt(256)));
        g.drawLine(nX1, nY1, nX2, nY2);
    }

    @Override
    // debris radiates away from center of foe
    public void move() {
        int midX1 = (nX1 + nX2) / 2;
        int midY1 = (nY1 + nY2) / 2;

        double angle = Math.atan((mCenter.getY() - midY1)/(mCenter.getX() - midX1));

        double changeX = 3 * Math.cos(angle);
        double changeY = 3 * Math.sin(angle);

        if (changeX == 0) {
            changeX = 3;
        }

        if (changeY == 0) {
            changeY = 3;
        }


        if (nX1 < mCenter.getX()) {
            nX1 -= Math.abs(changeX);
            nX2 -= Math.abs(changeX);
        } else {
            nX1 += Math.abs(changeX);
            nX2 += Math.abs(changeX);
        }

        if (nY1 < mCenter.getY()) {
            nY1 -= Math.abs(changeY);
            nY2 -= Math.abs(changeY);
        } else {
            nY1 += Math.abs(changeY);
            nY2 += Math.abs(changeY);
        }

        if (mExpiry == 0) {
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        } else {
            mExpiry--;
        }
    }

    // collision detection
    @Override
    public Point getCenter() {
        return null;
    }

    @Override
    public int getRadius() {
        return 0;
    }

    @Override
    public Team getTeam() {
        return Team.DEBRIS;
    }
}
