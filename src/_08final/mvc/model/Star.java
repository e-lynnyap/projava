package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;

/**
 * Created by elynn on 11/30/16.
 */
public class Star extends Sprite {
    public Star() {
        super();
        setCenter(new Point(Game.R.nextInt((int)Game.DIM.getWidth()), Game.R.nextInt((int)Game.DIM.getHeight())));
        setTeam(Team.STAR);
        setColor(new Color(Game.R.nextInt(256), Game.R.nextInt(256), Game.R.nextInt(256)));
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        g.fillOval((int)getCenter().getX(), (int)getCenter().getY(), 3, 3);
    }

    @Override
    public void move() {

    }
}
