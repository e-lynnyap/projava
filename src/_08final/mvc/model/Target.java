package _08final.mvc.model;

import java.awt.*;

/**
 * Created by elynn on 11/21/16.
 */
public class Target implements Movable {

    public static final int EXPIRATION = 1;
    public static final int RAD = 30;
    private Point p;
    private int expiry;

    public Target(Point p) {
        this.p = p;
        expiry = EXPIRATION;
    }

    @Override
    public void move() {

    }

    @Override
    public void draw(Graphics g) {
        if (expiry == 0) {
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        } else {
            expiry--;
        }

        g.setColor(Color.cyan);
        g.drawOval(p.x - RAD, p.y - RAD, 2 * RAD, 2 * RAD);
        g.drawLine(p.x, p.y - RAD, p.x, p.y + RAD); // horizontal line
        g.drawLine(p.x - RAD, p.y, p.x + RAD, p.y); // vertical line
    }






    @Override
    public Point getCenter() {
        return null;
    }

    @Override
    public int getRadius() {
        return 0;
    }

    @Override
    public Team getTeam() {
        return Team.DEBRIS;
    }
}
