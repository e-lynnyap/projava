package _08final.mvc.model;

import java.awt.*;

/**
 * Created by elynn on 11/27/16.
 */
public class LifeFloater extends Floater {
    public LifeFloater() {
        super();
        setColor(Color.YELLOW);
    }

    public LifeFloater(int nRadius, Point point) {
        super(nRadius, point);
        setColor(Color.YELLOW);
    }

    @Override
    public int getScore() {
        return 100;
    }
}
