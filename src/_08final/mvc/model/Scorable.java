package _08final.mvc.model;

/**
 * Created by elynn on 11/21/16.
 */
public interface Scorable {
    int getScore();
}
