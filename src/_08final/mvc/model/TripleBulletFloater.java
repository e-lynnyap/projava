package _08final.mvc.model;

import java.awt.*;

/**
 * Floater that gives power-up of shooting bullets in three directions
 */
public class TripleBulletFloater extends Floater implements Limitable{
    public TripleBulletFloater() {
        super();
        setColor(Color.RED);
    }

    public TripleBulletFloater(int nRadius, Point point) {
        super(nRadius, point);
        setColor(Color.RED);
    }

    @Override
    public int getScore() {
        return 200;
    }

    @Override
    public int getLimit() {
        return 300;
    }
}
