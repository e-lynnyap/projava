package _08final.mvc.model;

import _08final.mvc.controller.Game;
import _08final.sounds.Sound;

import java.util.ArrayList;
import java.util.List;


public class CommandCenter {

	private  int nNumFalcon;
	private  int nLevel;
	private  long lScore;
	private  Falcon falShip;
	private  boolean bPlaying;
	private  boolean bPaused;

	private int nPowerUpLimit;
	private int nShields; // number of shield uses remaining
	private int nTeleports; // number of teleport uses remaining

	// These ArrayLists with capacities set
	private List<Movable> movDebris = new ArrayList<Movable>(300);

	private List<Movable> movFriends = new ArrayList<Movable>(100);
	private List<Movable> movFoes = new ArrayList<Movable>(200);
	private List<Movable> movFloaters = new ArrayList<Movable>(50); // only interacts with friends
	private List<Movable> movPortals = new ArrayList<>(5);


	private List<Movable> movStars = new ArrayList<>(200);

	private GameOpsList opsList = new GameOpsList();
	private static CommandCenter instance = null;

	private int red = 224;
	private int blue = 224;
	private int green = 224;

	// Constructor made private - static Utility class only
	private CommandCenter() {}


	public static CommandCenter getInstance(){
		if (instance == null){
			instance = new CommandCenter();
		}
		return instance;
	}

	public  void initGame(){
		setLevel(1);
		setScore(0);
		setNumFalcons(15); // number of lives
		spawnFalcon(true);
	}


	// The parameter is true if this is for the beginning of the game, otherwise false
	// When you spawn a new falcon, you need to decrement its number
	public  void spawnFalcon(boolean bFirst) {
		if (getNumFalcons() != 0) {
			falShip = new Falcon();

			opsList.enqueue(falShip, CollisionOp.Operation.ADD);
			if (!bFirst)
			    setNumFalcons(getNumFalcons() - 1);
		}

		Sound.playSound("shipspawn.wav");

	}


	public GameOpsList getOpsList() {
		return opsList;
	}

	public void setOpsList(GameOpsList opsList) {
		this.opsList = opsList;
	}

	public int getPowerUpLimit() {
		return nPowerUpLimit;
	}

	public void setPowerUpLimit(int nPowerUpRemainingLimit) {
		this.nPowerUpLimit = nPowerUpRemainingLimit;
	}

	public  void clearAll(){
		movDebris.clear();
		movFriends.clear();
		movFoes.clear();
		movFloaters.clear();
	}

	public  boolean isPlaying() {
		return bPlaying;
	}

	public  void setPlaying(boolean bPlaying) {
		this.bPlaying = bPlaying;
	}

	public  boolean isPaused() {
		return bPaused;
	}

	public  void setPaused(boolean bPaused) {
		this.bPaused = bPaused;
	}

	public  boolean isGameOver() {		//if the number of falcons is zero, then game over
		if (getNumFalcons() == 0) {
			return true;
		}
		return false;
	}

	public  int getLevel() {
		return nLevel;
	}

	public   long getScore() {
		return lScore;
	}

	public  void setScore(long lParam) {
		lScore = lParam;
	}

	public void increaseScore(long lNewScore) {
		lScore += lNewScore;
	}

	public  void setLevel(int n) {
		nLevel = n;
	}

	public  int getNumFalcons() {
		return nNumFalcon;
	}

	public  void setNumFalcons(int nParam) {
		nNumFalcon = nParam;
	}

	public  Falcon getFalcon(){
		return falShip;
	}

	public  void setFalcon(Falcon falParam){
		falShip = falParam;
	}

	public  List<Movable> getMovDebris() {
		return movDebris;
	}

	public  List<Movable> getMovFriends() {
		return movFriends;
	}



	public  List<Movable> getMovFoes() {
		return movFoes;
	}



	public  List<Movable> getMovFloaters() {
		return movFloaters;
	}


	public List<Movable> getMovPortals() {
		return movPortals;
	}


	public List<Movable> getMovStars() {
		return movStars;
	}

	public int getShields() {
		return nShields;
	}

	public void setShields(int nShields) {
		this.nShields = nShields;
	}

	public int getTeleports() {
		return nTeleports;
	}

	public void setTeleports(int nTeleports) {
		this.nTeleports = nTeleports;
	}

	public int getRed() {
		return red;
	}

	public int getGreen() {
		return green;
	}

	public int getBlue() {
		return blue;
	}


	public void setRed(int red) {
		this.red = red;
	}

	public void setGreen(int green) {
		this.green = green;
	}

	public void setBlue(int blue) {
		this.blue = blue;
	}

}
