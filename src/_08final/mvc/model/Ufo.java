package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by elynn on 11/25/16.
 */
public class Ufo extends Sprite implements Scorable{

    private int level; // level 1 ufo moves left-to-right, level 2 ufo moves towards falcon

    private int health; // gets destroyed when health goes down to 0

    private int timer;

    private boolean towardsFalcon; // for level 3 ufo, indicates if moving towards or away

    public Ufo(int level) {
        super();
        double dX = getCenter().getX();
        double dY = getCenter().getY();

        if (dY + getRadius() > Game.DIM.getHeight()) {
            setCenter(new Point((int)dX, (int)(dY - getRadius())));
        } else if (dY - getRadius() < 0) {
            setCenter(new Point((int)dX, (int)(dY + getRadius())));
        }

        if (level == 1) {
            this.health = 3;
            setColor(new Color(0, 76, 153));
            setRadius(60);
        } else if (level == 2) {
            this.health = 4;
            setColor(new Color(76, 0, 153));
            setRadius(45);
        } else if (level == 3) {
            this.health = 5;
            setColor(new Color(0, 102, 0));
            setRadius(35);
        }

        setTeam(Team.FOE);

        this.level = level;

        assignPolarPoints(outline());

        int nX = Game.R.nextInt(10);

        //set random DeltaX
        if (nX % 2 == 0)
            setDeltaX(nX);
        else
            setDeltaX(-nX);

        // level 2 ufo also moves up and down randomly
        if (level == 2 || level == 3) {
            int nY = Game.R.nextInt(10);
            //random delta-y
            int nDY = Game.R.nextInt(10);
            if(nDY %2 ==0)
                nDY = -nDY;
            setDeltaY(nDY);
        }

        setOrientation(90);

    }

    public Ufo(Ufo ufoHit) {
        this(ufoHit.getLevel() + 1);
        setCenter(ufoHit.getCenter());
    }

    @Override
    public void move() {

        // both level 1 and 2 ufos move from left to right randomly
        if (level == 1 || level == 2) {
            double dX = getDeltaX();
            if (dX > 0) {
                dX += 0.25;
            } else {
                dX -= 0.25;
            }
            if (dX > Game.R.nextInt(18) + 10) {
                dX = -10;
            } else if (dX < -(Game.R.nextInt(18) + 10)) {
                dX = Game.R.nextInt(10);
            }
            setDeltaX(dX);

            // level 2 ufo also moves up and down randomly
            if (level == 2) {
                double dY = getDeltaY();
                if (dY > 0) {
                    dY += 0.25;
                } else {
                    dY -= 0.25;
                }
                if (dY > Game.R.nextInt(5) + 1) {
                    dY = -5;
                } else if (dY < - (Game.R.nextInt(5) + 11)) {
                    dY = Game.R.nextInt(5);
                }
                setDeltaY(dY);
            }
        }

        // level 3 ufo moves towards falcon, then away, then towards...
        if (level == 3) {
            timer++;


            double falconX = CommandCenter.getInstance().getFalcon().getCenter().getX();
            double falconY = CommandCenter.getInstance().getFalcon().getCenter().getY();
            double ufoX = this.getCenter().getX();
            double ufoY = this.getCenter().getY();
            double dist = Math.sqrt(Math.pow((falconY - ufoY), 2) + Math.pow((falconX - ufoX), 2) ); // dist between ufo and falcon

            double xChange = Game.R.nextInt(3) + 1;
            double yChange = Game.R.nextInt(3) + 1;

            if (timer % 100 == 0 || dist < 100 || ufoX > getDim().width || ufoY > getDim().height ) {
                towardsFalcon = !towardsFalcon; // reverse direction
            }

            if (timer % 20 == 0) {
                // fire a bullet
                CommandCenter.getInstance().getOpsList().enqueue(new UfoBullet(this), CollisionOp.Operation.ADD);
            }

            if (towardsFalcon) {
                if (ufoX < falconX) {
                    setDeltaX(xChange);
                } else {
                    setDeltaX(-xChange);
                }

                if (ufoY < falconY) {
                    setDeltaY(yChange);
                } else {
                    setDeltaY(-yChange);
                }
            } else {
                if (ufoX > falconX) {
                    setDeltaX(xChange);
                } else {
                    setDeltaX(-xChange);
                }

                if (ufoY > falconY) {
                    setDeltaY(yChange);
                } else {
                    setDeltaY(-yChange);
                }
            }
        }

        super.move();
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        //fill this polygon (with whatever color it has)
        g.fillPolygon(getXcoords(), getYcoords(), dDegrees.length);
        //now draw a white border
        g.setColor(java.awt.Color.WHITE);
        g.drawPolygon(getXcoords(), getYcoords(), dDegrees.length);
    }

    @Override
    public int getScore() {
        switch (this.level) {
            case 1:
                return 150;
            case 2:
                return 300;
            case 3:
                return 450;
            default:
                return 0;
        }
    }

    private ArrayList<Point> outline() {
        ArrayList<Point> pntCs = new ArrayList<Point>();

        pntCs.add(new Point(-15, 0));
        pntCs.add(new Point(-10, -5));
        pntCs.add(new Point(-5, -5));
        pntCs.add(new Point(-5, -8));
        pntCs.add(new Point(5, -8));
        pntCs.add(new Point(5, -5));
        pntCs.add(new Point(10, -5));
        pntCs.add(new Point(15, 0));
        pntCs.add(new Point(10, 5));
        pntCs.add(new Point(-10, 5));

        return pntCs;

    }

    public int getHealth() {
        return health;
    }

    public void decreaseHealth() {
        setColor(getColor().brighter());
        health--;
    }
}
