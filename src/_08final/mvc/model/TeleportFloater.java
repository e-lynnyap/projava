package _08final.mvc.model;

import java.awt.*;

/**
 * Created by elynn on 11/24/16.
 */
public class TeleportFloater extends Floater {
    public TeleportFloater() {
        super();
        setColor(Color.pink);
    }

    public TeleportFloater(int nRadius, Point point) {
        super(nRadius, point);
        setColor(Color.pink);
    }

    @Override
    public int getScore() {
        return 100;
    }
}
