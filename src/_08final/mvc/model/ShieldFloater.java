package _08final.mvc.model;

import java.awt.*;

/**
 * Created by elynn on 11/24/16.
 */
public class ShieldFloater extends Floater {
    public ShieldFloater() {
        super();
        setColor(Color.CYAN);
    }

    public ShieldFloater(int nRadius, Point point) {
        super(nRadius, point);
        setColor(Color.CYAN);
    }

    @Override
    public int getScore() {
        return 100;
    }
}
