package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;


public class UfoBullet extends Sprite {

	private final double FIRE_POWER = 10.0;

	public UfoBullet(Ufo ufo) {

		super();
		setTeam(Team.FOE);

		setColor(ufo.getColor());

		//defined the points on a cartesean grid
		ArrayList<Point> pntCs = new ArrayList<Point>();

		pntCs.add(new Point(0,3)); //top point

		pntCs.add(new Point(1,-1));
		pntCs.add(new Point(0,-2));
		pntCs.add(new Point(-1,-1));

		assignPolarPoints(pntCs);

		//a bullet expires after 20 frames
		setExpire( 20 );
		setRadius(8);


		double xChange = FIRE_POWER;
		double yChange = FIRE_POWER;
		double ufoX = ufo.getCenter().getX();
		double ufoY = ufo.getCenter().getY();
		double falconX = CommandCenter.getInstance().getFalcon().getCenter().getX();
		double falconY = CommandCenter.getInstance().getFalcon().getCenter().getY();

		double angleRadians = Math.atan2((falconY - ufoY), (falconX - ufoX));

		if (ufoX < falconX) {
			setDeltaX(xChange);
		} else {
			setDeltaX(-xChange);
		}

		if (ufoY < falconY) {
			setDeltaY(yChange);
		} else {
			setDeltaY(-yChange);
		}

		//everything is relative to the ufo that fired the bullet
		setCenter( ufo.getCenter() );

		setOrientation((int)Math.toDegrees(angleRadians)); // aim the bullet at the falcon

	}

	@Override
	public void move(){

		super.move();

		if (getExpire() == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
		}
		else {
			setExpire(getExpire() - 1);
		}

	}

}
