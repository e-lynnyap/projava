package _08final.mvc.controller;

import _08final.mvc.model.*;
import _08final.mvc.view.GamePanel;
import _08final.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Random;

// ===============================================
// == This Game class is the CONTROLLER
// ===============================================

public class Game implements Runnable, KeyListener {

	// ===============================================
	// FIELDS
	// ===============================================

	public static final Dimension DIM = new Dimension(1100, 800); //the dimension of the game.
	private GamePanel gmpPanel;
	public static Random R = new Random();
	public final static int ANI_DELAY = 45; // milliseconds between screen
											// updates (animation)
	private Thread thrAnim;
	private int nLevel = 1;
	private int nTick = 0;

	private boolean bMuted = true;
	

	private final int PAUSE = 80, // p key
			QUIT = 81, // q key
			LEFT = 37, // rotate left; left arrow
			RIGHT = 39, // rotate right; right arrow
			UP = 38, // thrust; up arrow
			START = 83, // s key
			FIRE = 32, // space key
			MUTE = 77, // m-key mute

	// for possible future use
	TELEPORT = 68, 					// d key
	 SHIELD = 65, 				// a key arrow
	 NUM_ENTER = 10, 				// hyp
	 SPECIAL = 70; 					// fire special weapon;  F key

	private Clip clpThrust;
	private Clip clpMusicBackground;

	private boolean bTripleBullets; // if true, then pressing fire will shoot bullets in 3 directions

	private boolean bLimitablePowerUpInProgress; // indicates if a time-limited power-up is currently in play

	private boolean bPortalOpen = false;

	// time intervals for floater spawning: the lower the number the more frequently it appears
	private static final int SPAWN_NEW_SHIP_FLOATER = 700;
	private static final int SPAWN_LIFE_FLOATER = 300;
	private static final int SPAWN_TRIPLE_BULLET_FLOATER = 650;
	private static final int SPAWN_SHIELD_FLOATER = 600;
	private static final int SPAWN_TELEPORT_FLOATER = 620;

	private static final int NUM_STARS = 250; // stars in background


	// ===============================================
	// ==CONSTRUCTOR
	// ===============================================

	public Game() {

		gmpPanel = new GamePanel(DIM);
		gmpPanel.addKeyListener(this);
		clpThrust = Sound.clipForLoopFactory("whitenoise.wav");
		clpMusicBackground = Sound.clipForLoopFactory("music-background.wav");
	

	}

	// ===============================================
	// ==METHODS
	// ===============================================

	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
			public void run() {
				try {
					Game game = new Game(); // construct itself
					game.fireUpAnimThread();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void fireUpAnimThread() { // called initially
		if (thrAnim == null) {
			thrAnim = new Thread(this); // pass the thread a runnable object (this)
			thrAnim.start();
		}
	}

	// implements runnable - must have run method
	public void run() {

		// lower this thread's priority; let the "main" aka 'Event Dispatch'
		// thread do what it needs to do first
		thrAnim.setPriority(Thread.MIN_PRIORITY);

		// and get the current time
		long lStartTime = System.currentTimeMillis();

		// this thread animates the scene
		while (Thread.currentThread() == thrAnim) {
			tick();
			spawnFloaters();
			checkTimeLimit(); // checks the time limit for power-ups in progress
			gmpPanel.update(gmpPanel.getGraphics()); // update takes the graphics context we must 
														// surround the sleep() in a try/catch block
														// this simply controls delay time between 
														// the frames of the animation

			//this might be a good place to check for collisions
			checkCollisions();

			//this might be a good place to check if the level is clear (no more foes)
			//if the level is clear then spawn some big asteroids -- the number of asteroids 
			//should increase with the level. 
			checkNewLevel();

			try {
				// The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update) 
				// between frames takes longer than ANI_DELAY, then the difference between lStartTime - 
				// System.currentTimeMillis() will be negative, then zero will be the sleep time
				lStartTime += ANI_DELAY;
				Thread.sleep(Math.max(0,
						lStartTime - System.currentTimeMillis()));
			} catch (InterruptedException e) {
				// just skip this frame -- no big deal
				continue;
			}
		} // end while
	} // end run

	private void checkCollisions() {

		Point pntFriendCenter, pntFoeCenter;
		int nFriendRadiux, nFoeRadiux;

		for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
			for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {

				pntFriendCenter = movFriend.getCenter();
				pntFoeCenter = movFoe.getCenter();
				nFriendRadiux = movFriend.getRadius();
				nFoeRadiux = movFoe.getRadius();

				//detect collision between friend and foe
				if (pntFriendCenter.distance(pntFoeCenter) < (nFriendRadiux + nFoeRadiux)) {

					//falcon
					if ((movFriend instanceof Falcon) ){
						// if falcon is not protected, remove falcon
						if (((Falcon) movFriend).isOriginal()) {
							if (!CommandCenter.getInstance().getFalcon().getProtected()) {
								CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
								CommandCenter.getInstance().spawnFalcon(false);
							}
						}
					}
					//not falcon
					else {
						CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
					}//end else
					//kill the foe and if asteroid, then spawn new asteroids
					killFoe(movFoe);

					if (movFoe instanceof Asteroid) {
						Sound.playSound("kapow.wav");
					} else if (movFoe instanceof Ufo) {
						Sound.playSound("ufoHit.wav");
					}

					if (movFriend instanceof Falcon && ((Falcon)movFriend).isOriginal()) {
						if (!((Falcon)movFriend).getProtected()) {
							Sound.playSound("hurt.wav"); // only play this sound if the original falcon is hit by foe
						}
					}

				}//end if 
			}//end inner for
		}//end outer for


		if (CommandCenter.getInstance().getFalcon() != null){
			Point pntFalCenter = CommandCenter.getInstance().getFalcon().getCenter();
			int nFalRadiux = CommandCenter.getInstance().getFalcon().getRadius();
			Point pntFloaterCenter;
			int nFloaterRadiux;

			//check for collisions between falcon and floaters
			for (Movable movFloater : CommandCenter.getInstance().getMovFloaters()) {
				pntFloaterCenter = movFloater.getCenter();
				nFloaterRadiux = movFloater.getRadius();
	
				//detect collision
				if (pntFalCenter.distance(pntFloaterCenter) < (nFalRadiux + nFloaterRadiux)) {
					if (movFloater instanceof NewShipFloater) { // create a new falcon
						CommandCenter.getInstance().getOpsList().enqueue(new Falcon(true), CollisionOp.Operation.ADD);
					} else if (movFloater instanceof TripleBulletFloater) { // fire three bullets
						bTripleBullets = true;
					} else if (movFloater instanceof ShieldFloater) { // increment shield uses
						CommandCenter.getInstance().setShields(CommandCenter.getInstance().getShields() + 1);
					} else if (movFloater instanceof TeleportFloater) { // increment teleport uses
						CommandCenter.getInstance().setTeleports(CommandCenter.getInstance().getTeleports() + 1);
					} else if (movFloater instanceof LifeFloater) { // give an extra life
						CommandCenter.getInstance().setNumFalcons(CommandCenter.getInstance().getNumFalcons() + 1);
					}

					if (movFloater instanceof Scorable) {
						CommandCenter.getInstance().increaseScore(((Scorable)movFloater).getScore());
					}
					if (movFloater instanceof Limitable) {
						bLimitablePowerUpInProgress = true;
						CommandCenter.getInstance().setPowerUpLimit(((Limitable) movFloater).getLimit()); // set the time limit for the powerup
						removeLimitableFloaters();
					}

					CommandCenter.getInstance().getOpsList().enqueue(movFloater, CollisionOp.Operation.REMOVE);


					Sound.playSound("pacman_eatghost.wav");
	
				}//end if 
			}//end inner for

			Point pntPortalCenter;
			int nPortalRadiux;

			//check for collision between falcon and portal
			for (Movable movPortal : CommandCenter.getInstance().getMovPortals()) {
				pntPortalCenter = movPortal.getCenter();
				nPortalRadiux = movPortal.getRadius();

				//detect collision
				if (pntFalCenter.distance(pntPortalCenter) < (nFalRadiux + nPortalRadiux)) {
					startNewLevel(); // start the next level and close the portal
					bPortalOpen = false;
					Sound.playSound("teleport.wav");
					CommandCenter.getInstance().getOpsList().enqueue(movPortal, CollisionOp.Operation.REMOVE);
				}
			}
		}//end if not null
		


		//we are dequeuing the opsList and performing operations in serial to avoid mutating the movable arraylists while iterating them above
		while(!CommandCenter.getInstance().getOpsList().isEmpty()){
			CollisionOp cop =  CommandCenter.getInstance().getOpsList().dequeue();
			Movable mov = cop.getMovable();
			CollisionOp.Operation operation = cop.getOperation();

			switch (mov.getTeam()){
				case FOE:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFoes().add(mov);
					} else {
						CommandCenter.getInstance().getMovFoes().remove(mov);
					}

					break;
				case FRIEND:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFriends().add(mov);
					} else {
						CommandCenter.getInstance().getMovFriends().remove(mov);
					}
					break;

				case FLOATER:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFloaters().add(mov);
					} else {
						CommandCenter.getInstance().getMovFloaters().remove(mov);
					}
					break;

				case DEBRIS:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovDebris().add(mov);
					} else {
						CommandCenter.getInstance().getMovDebris().remove(mov);
					}
					break;

				case PORTAL:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovPortals().add(mov);
					} else {
						CommandCenter.getInstance().getMovPortals().remove(mov);
					}
					break;

				case STAR:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovStars().add(mov);
					} else {
						CommandCenter.getInstance().getMovStars().remove(mov);
					}
					break;

			}

		}
		//a request to the JVM is made every frame to garbage collect, however, the JVM will choose when and how to do this
		System.gc();
		
	}//end meth

	// checks if time limit has been reached for any power-ups in play and if so, removes the effect of that power-up
	private void checkTimeLimit() {
		if (CommandCenter.getInstance().getPowerUpLimit() > 0) {
			CommandCenter.getInstance().setPowerUpLimit(CommandCenter.getInstance().getPowerUpLimit() - 1);
		} else {
			removeLimitablePowerUps();
		}
	}

	private void removeLimitablePowerUps() {
		bLimitablePowerUpInProgress = false;
		// cancel out all power-ups
		bTripleBullets = false;
		for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
            if (movFriend instanceof Falcon) {
                if (!((Falcon)movFriend).isOriginal()) {
                    CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
                }
            }
        }
	}

	private void killFoe(Movable movFoe) {

		if (movFoe instanceof Scorable) {
			CommandCenter.getInstance().increaseScore(((Scorable)movFoe).getScore()); // add to score
		}
		
		if (movFoe instanceof Asteroid){

			//we know this is an Asteroid, so we can cast without threat of ClassCastException
			Asteroid astExploded = (Asteroid)movFoe;
			//big asteroid 
			if(astExploded.getSize() == 0){
				//spawn two medium Asteroids
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
			} 
			//medium size aseroid exploded
			else if(astExploded.getSize() == 1){
				//spawn three small Asteroids
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
			}

			try {
				produceDebris(movFoe);
			} catch (NullPointerException e) {
				produceDebris(CommandCenter.getInstance().getFalcon());
			}

			//remove the original Foe
			CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);
		}

		if (movFoe instanceof Ufo) {
			Ufo ufoHit = (Ufo)movFoe;

			if (ufoHit.getHealth() > 1) {
				ufoHit.decreaseHealth(); // each hit depletes ufo health until it goes to 0; only then is ufo destroyed
			} else { // ufo health depleted
				produceDebris(movFoe); // only produce debris if ufo is destroyed
				CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);
				if (ufoHit.getLevel() == 1) { // spawn a level 2 ufo in its place
					CommandCenter.getInstance().getOpsList().enqueue(new Ufo(ufoHit), CollisionOp.Operation.ADD);
				} else if (ufoHit.getLevel() == 2) { // spawn a level 3 ufo
					CommandCenter.getInstance().getOpsList().enqueue(new Ufo(ufoHit), CollisionOp.Operation.ADD);
				}
			}
		}

	}

	//some methods for timing events in the game,
	//such as the appearance of UFOs, floaters (power-ups), etc. 
	public void tick() {
		if (nTick == Integer.MAX_VALUE)
			nTick = 0;
		else
			nTick++;
	}

	public int getTick() {
		return nTick;
	}

	// produce debris when foe is destroyed
	private void produceDebris(Movable mov) {
		Point center = mov.getCenter();
		Point[] pntCoords = ((Sprite)mov).getObjectPoints();
		int j;

		// each debris sprite is a single line between two adjacent points in the foe's coords
		for (int i = 0; i < pntCoords.length; i++) {
			if (i == pntCoords.length-1) {
				j = 0;
			} else {
				j = i + 1;
			}
			CommandCenter.getInstance().getOpsList().enqueue(
					new Debris(pntCoords[i], pntCoords[j], center), CollisionOp.Operation.ADD);
		}
	}

	private void spawnFloaters() {

		if (!bPortalOpen) { // only spawn floaters when portal is closed
			//make the appearance of power-up dependent upon ticks and levels
			//the higher the level the more frequent the appearance
			if (nTick % (SPAWN_SHIELD_FLOATER - nLevel * 7) == 0) {
				CommandCenter.getInstance().getOpsList().enqueue(new ShieldFloater(), CollisionOp.Operation.ADD);
			}

			if (nTick % (SPAWN_LIFE_FLOATER - nLevel * 7) == 0) {
				CommandCenter.getInstance().getOpsList().enqueue(new LifeFloater(), CollisionOp.Operation.ADD);
			}

			if (nTick % (SPAWN_TELEPORT_FLOATER - nLevel * 7) == 0) {
				CommandCenter.getInstance().getOpsList().enqueue(new TeleportFloater(), CollisionOp.Operation.ADD);
			}

			// only spawn these floaters if there are no time-limited power-ups in progress
			if (!bLimitablePowerUpInProgress) {
				if (nTick % (SPAWN_TRIPLE_BULLET_FLOATER - nLevel * 7) == 0) {
					CommandCenter.getInstance().getOpsList().enqueue(new TripleBulletFloater(), CollisionOp.Operation.ADD);
				}
				if (nTick % (SPAWN_NEW_SHIP_FLOATER - nLevel * 7) == 0) {
					CommandCenter.getInstance().getOpsList().enqueue(new NewShipFloater(), CollisionOp.Operation.ADD);
				}
			}
		}
	}


	// remove all floaters that are time-limited
	private void removeLimitableFloaters() {
		for (Movable movFloater : CommandCenter.getInstance().getMovFloaters()) {
			if (movFloater instanceof Limitable) {
				CommandCenter.getInstance().getOpsList().enqueue(movFloater, CollisionOp.Operation.REMOVE);
			}
		}
	}


	// Called when user presses 's'
	private void startGame() {
		CommandCenter.getInstance().clearAll();
		CommandCenter.getInstance().initGame();
		CommandCenter.getInstance().setLevel(0);
		CommandCenter.getInstance().setShields(2); // start with 2 shields
		CommandCenter.getInstance().setTeleports(2); // start with 2 teleports
		CommandCenter.getInstance().setPlaying(true);
		CommandCenter.getInstance().setPaused(false);
		//if (!bMuted)
		   // clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
	}

	//this method spawns new asteroids
	private void spawnAsteroids(int nNum) {
		for (int nC = 0; nC < nNum; nC++) {
			//Asteroids with size of zero are big
			CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(0), CollisionOp.Operation.ADD);

		}
	}

	// spawn new UFOs
	private void spawnUfos(int nNum) {
		for (int nC = 0; nC < nNum; nC++) {
			// Ufo of level 1 move left to right
			CommandCenter.getInstance().getOpsList().enqueue(new Ufo(1), CollisionOp.Operation.ADD);
		}
	}
	
	
	private boolean isLevelClear(){
		//if there are no more Asteroids on the screen
		boolean bAsteroidFree = true;

		//if there are no more Ufos on the screen
		boolean bUfoFree = true;

		for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
			if (movFoe instanceof Asteroid){
				bAsteroidFree = false;
				break;
			} else if (movFoe instanceof Ufo) {
				bUfoFree = false;
				break;
			}
		}

		return (bAsteroidFree && bUfoFree);

		
	}
	
	private void checkNewLevel(){

		if (isLevelClear() ){
			if (CommandCenter.getInstance().getLevel() == 0) {
				startNewLevel();
				return;
			}

			if (CommandCenter.getInstance().getLevel() > 0 && !bPortalOpen) {
				bPortalOpen = true;
				Sound.playSound("portal.wav");
//				System.out.println("Level: " + CommandCenter.getInstance().getLevel());
//				System.out.println("Level clear: " + isLevelClear());
				CommandCenter.getInstance().getOpsList().enqueue(new Portal(), CollisionOp.Operation.ADD);
			}
		} else {
			bPortalOpen = false;
			for (Movable portal : CommandCenter.getInstance().getMovPortals()) {
				CommandCenter.getInstance().getOpsList().enqueue(portal, CollisionOp.Operation.REMOVE);
			}
		}
	}

	private void startNewLevel() {
		if (CommandCenter.getInstance().getFalcon() !=null) {

			// cancel out any power-ups still in play
			CommandCenter.getInstance().setPowerUpLimit(0);
			removeLimitablePowerUps();

			CommandCenter.getInstance().setLevel(CommandCenter.getInstance().getLevel() + 1);
			spawnAsteroids(CommandCenter.getInstance().getLevel() / 2 + 1); // fewer asteroids
			spawnUfos(CommandCenter.getInstance().getLevel() / 2);
			CommandCenter.getInstance().getFalcon().setProtected(true);
			setBackgroundColor();

			// remove old stars
			for (Movable movStar : CommandCenter.getInstance().getMovStars()) {
				CommandCenter.getInstance().getOpsList().enqueue(movStar, CollisionOp.Operation.REMOVE);
			}

			// add new stars
			for (int nC = 0; nC < NUM_STARS; nC++) {
				CommandCenter.getInstance().getOpsList().enqueue(new Star(), CollisionOp.Operation.ADD);
			}
		}
	}


	// Varargs for stopping looping-music-clips
	private static void stopLoopingSounds(Clip... clpClips) {
		for (Clip clp : clpClips) {
			clp.stop();
		}
	}

	public void setBackgroundColor() {
		CommandCenter.getInstance().setRed(Game.R.nextInt(15) + 210);
		CommandCenter.getInstance().setGreen(Game.R.nextInt(15) + 210);
		CommandCenter.getInstance().setBlue(Game.R.nextInt(15) + 210);
	}

	// ===============================================
	// KEYLISTENER METHODS
	// ===============================================

	@Override
	public void keyPressed(KeyEvent e) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		int nKey = e.getKeyCode();
		// System.out.println(nKey);

		if (nKey == START && !CommandCenter.getInstance().isPlaying())
			startGame();

		if (fal != null) {

			switch (nKey) {
			case PAUSE:
				CommandCenter.getInstance().setPaused(!CommandCenter.getInstance().isPaused());
				if (CommandCenter.getInstance().isPaused())
					stopLoopingSounds(clpMusicBackground, clpThrust);
				else
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
				break;
			case QUIT:
				System.exit(0);
				break;
			case UP:
				fal.thrustOn();
				if (!CommandCenter.getInstance().isPaused())
					clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
				break;
			case LEFT:
				fal.rotateLeft();
				break;
			case RIGHT:
				fal.rotateRight();
				break;

			// possible future use
			// case KILL:
			// case SHIELD:
			// case NUM_ENTER:

			default:
				break;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		int nKey = e.getKeyCode();
		 System.out.println(nKey);

		if (fal != null) {
			switch (nKey) {
			case FIRE:
				CommandCenter.getInstance().getOpsList().enqueue(new Bullet(fal), CollisionOp.Operation.ADD);
				if (bTripleBullets) {
					CommandCenter.getInstance().getOpsList().enqueue(new Bullet(fal, 7), CollisionOp.Operation.ADD);
					CommandCenter.getInstance().getOpsList().enqueue(new Bullet(fal, -7), CollisionOp.Operation.ADD);
					Sound.playSound("triple-shoot.wav");
				} else {
					Sound.playSound("laser.wav");
				}


				break;
				
			//special is a special weapon, current it just fires the cruise missile. 
			case SPECIAL:
				CommandCenter.getInstance().getOpsList().enqueue(new Cruise(fal), CollisionOp.Operation.ADD);
				//Sound.playSound("laser.wav");
				break;
				
			case LEFT:
				fal.stopRotating();
				break;
			case RIGHT:
				fal.stopRotating();
				break;
			case UP:
				fal.thrustOff();
				clpThrust.stop();
				break;
				
			case MUTE:
				if (!bMuted){
					stopLoopingSounds(clpMusicBackground);
					bMuted = !bMuted;
				} 
				else {
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
					bMuted = !bMuted;
				}
				break;

			case TELEPORT:
				if (CommandCenter.getInstance().getTeleports() > 0) { // can only use if teleport count is positive
					CommandCenter.getInstance().setTeleports(CommandCenter.getInstance().getTeleports() - 1);

					// wherever the falcon is, set its x and y to some random point
					Point point = new Point(
							Game.R.nextInt((int) Game.DIM.getWidth()),
							Game.R.nextInt((int) Game.DIM.getHeight()));
					CommandCenter.getInstance().getFalcon().setCenter(point);
					Sound.playSound("teleport_2.wav");
					CommandCenter.getInstance().getFalcon().setProtected(true); // falcon is momentarily protected
				}
				break;

			case SHIELD:

				if (CommandCenter.getInstance().getShields() > 0) { // only works if shield count is positive
					CommandCenter.getInstance().setShields(CommandCenter.getInstance().getShields() - 1);
					Sound.playSound("shieldup.wav");
					for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
						if (movFriend instanceof Falcon) {
							((Falcon) movFriend).setProtected(true);
						}
					}
				}
				break;


			case NUM_ENTER:
				// get all foes and destroy them
				for (Movable movable : CommandCenter.getInstance().getMovFoes()) {
					killFoe(movable);
				}

				break;
			default:
				break;
			}

		}
	}

	@Override
	// Just need it b/c of KeyListener implementation
	public void keyTyped(KeyEvent e) {
	}
}


