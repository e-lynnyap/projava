package _08final.mvc.view;

import _08final.mvc.controller.Game;
import _08final.mvc.model.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class GamePanel extends Panel {
	
	// ==============================================================
	// FIELDS 
	// ============================================================== 
	 
	// The following "off" vars are used for the off-screen double-bufferred image. 
	private Dimension dimOff;
	private Image imgOff;
	private Graphics grpOff;
	
	private GameFrame gmf;
	private Font fnt = new Font("SansSerif", Font.BOLD, 14);
	private Font fntBig = new Font("SansSerif", Font.BOLD + Font.ITALIC, 36);
	private FontMetrics fmt; 
	private int nFontWidth;
	private int nFontHeight;
	private String strDisplay = "";
	

	// ==============================================================
	// CONSTRUCTOR 
	// ==============================================================
	
	public GamePanel(Dimension dim){
	    gmf = new GameFrame();
		gmf.getContentPane().add(this);
		gmf.pack();
		initView();
		gmf.setSize(dim);
		gmf.setTitle("Game Base");
		gmf.setResizable(false);
		gmf.setVisible(true);
		this.setFocusable(true);
	}
	
	
	// ==============================================================
	// METHODS 
	// ==============================================================
	
	private void drawScore(Graphics g) {
		g.setColor(Color.BLACK);
		g.setFont(fnt);
		if (CommandCenter.getInstance().getScore() != 0) {
			g.drawString("SCORE :  " + CommandCenter.getInstance().getScore(), nFontWidth, nFontHeight);
		} else {
			g.drawString("NO SCORE", nFontWidth, nFontHeight);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void update(Graphics g) {
		if (grpOff == null || Game.DIM.width != dimOff.width
				|| Game.DIM.height != dimOff.height) {
			dimOff = Game.DIM;
			imgOff = createImage(Game.DIM.width, Game.DIM.height);
			grpOff = imgOff.getGraphics();
		}

		// Fill in background with color set for current level.
		grpOff.setColor(new Color(CommandCenter.getInstance().getRed(), CommandCenter.getInstance().getGreen(),
									CommandCenter.getInstance().getBlue()));
		grpOff.fillRect(0, 0, Game.DIM.width, Game.DIM.height);

		drawScore(grpOff);
		drawNumberShieldsLeft(grpOff);
		drawNumberTeleportsLeft(grpOff);
		drawFloaterLegend(grpOff);
		
		if (!CommandCenter.getInstance().isPlaying()) {
			displayTextOnScreen();
		} else if (CommandCenter.getInstance().isPaused()) {
			strDisplay = "Game Paused";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4);
		}
		
		//playing and not paused!
		else {
			
			//draw them in decreasing level of importance
			//friends will be on top layer and debris on the bottom
			iterateMovables(grpOff,
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovStars(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovFriends(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovFoes(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovFloaters(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovDebris(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovPortals());


			drawNumberShipsLeft(grpOff);
			drawPowerUpLimitMeter(grpOff);
			if (CommandCenter.getInstance().isGameOver()) {
				CommandCenter.getInstance().setPlaying(false);
				//bPlaying = false;
			}
		}
		//draw the double-Buffered Image to the graphics context of the panel
		g.drawImage(imgOff, 0, 0, this);
	} 


	
	//for each movable array, process it.
	private void iterateMovables(Graphics g, ArrayList<Movable>...movMovz){
		
		for (ArrayList<Movable> movMovs : movMovz) {
			for (Movable mov : movMovs) {

				mov.move();
				mov.draw(g);

			}
		}
		
	}
	

	// Draw the number of falcons left on the bottom-right of the screen. 
	private void drawNumberShipsLeft(Graphics g) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		double[] dLens = fal.getLengths();
		int nLen = fal.getDegrees().length;
		Point[] pntMs = new Point[nLen];
		int[] nXs = new int[nLen];
		int[] nYs = new int[nLen];
	
		//convert to cartesian points
		for (int nC = 0; nC < nLen; nC++) {
			pntMs[nC] = new Point((int) (10 * dLens[nC] * Math.sin(Math
					.toRadians(90) + fal.getDegrees()[nC])),
					(int) (10 * dLens[nC] * Math.cos(Math.toRadians(90)
							+ fal.getDegrees()[nC])));
		}

		g.setColor(Color.BLACK);
		//for each falcon left (not including the one that is playing)
		for (int nD = 1; nD < CommandCenter.getInstance().getNumFalcons(); nD++) {
			//create x and y values for the objects to the bottom right using cartesean points again
			for (int nC = 0; nC < fal.getDegrees().length; nC++) {
				nXs[nC] = pntMs[nC].x + Game.DIM.width - (20 * nD);
				nYs[nC] = pntMs[nC].y + Game.DIM.height - 40;
			}
			g.drawPolygon(nXs, nYs, nLen);
		} 
	}

	// shows how many shields are left to use
	private void drawNumberShieldsLeft(Graphics g) {
		// draw a shield icon beside the text
		ShieldFloater shieldFloater = new ShieldFloater(10, new Point(nFontWidth + 180, nFontHeight - 5));
		shieldFloater.draw(grpOff);

		g.setColor(Color.BLACK);
		g.setFont(fnt);
		g.drawString("Shields Left: " + CommandCenter.getInstance().getShields(), nFontWidth + 200, nFontHeight);
	}

	// shows how many teleports are left to use
	private void drawNumberTeleportsLeft(Graphics g) {
		// draw a teleport icon beside the text
		TeleportFloater teleportFloater = new TeleportFloater(10, new Point(nFontWidth + 360, nFontHeight - 5));
		teleportFloater.draw(grpOff);

		g.setColor(Color.BLACK);
		g.setFont(fnt);
		g.drawString("Teleports Left: " + CommandCenter.getInstance().getTeleports(), nFontWidth + 380, nFontHeight);
	}

	private void drawFloaterLegend(Graphics g) {
		g.setColor(Color.BLACK);
		g.setFont(fnt);
		g.drawString("Legend: ", nFontWidth + 550, nFontHeight);

		TripleBulletFloater tripleBulletFloater = new TripleBulletFloater(10, new Point(nFontWidth + 630, nFontHeight - 5));
		tripleBulletFloater.draw(grpOff);

		g.setColor(Color.BLACK);
		g.drawString("  Triple Bullets", nFontWidth + 640, nFontHeight);

		NewShipFloater newShipFloater = new NewShipFloater(10, new Point(nFontWidth + 785, nFontHeight - 5));
		newShipFloater.draw(grpOff);

		g.setColor(Color.BLACK);
		g.drawString("  Clone Falcon", nFontWidth + 795, nFontHeight);

		LifeFloater lifeFloater = new LifeFloater(10, new Point(nFontWidth + 940, nFontHeight - 5));
		lifeFloater.draw(grpOff);

		g.setColor(Color.BLACK);
		g.drawString("  Extra Life", nFontWidth + 950, nFontHeight);
	}

	// Draw the time limit for the power-up on the bottom-left of the screen.
	private void drawPowerUpLimitMeter(Graphics g) {
		g.setColor(Color.BLACK);
		g.setFont(fnt);
		int nLimit = CommandCenter.getInstance().getPowerUpLimit();
		if (nLimit > 0) {
			g.drawString("Power-Up Time Remaining: ", nFontWidth, Game.DIM.height - 2 * nFontHeight);
		}
		g.setColor(Color.GREEN);
		g.fillRect(240, Game.DIM.height - 50, nLimit, 40);
	}
	
	private void initView() {
		Graphics g = getGraphics();			// get the graphics context for the panel
		g.setFont(fnt);						// take care of some simple font stuff
		fmt = g.getFontMetrics();
		nFontWidth = fmt.getMaxAdvance();
		nFontHeight = fmt.getHeight();
		g.setFont(fntBig);					// set font info
	}
	
	// This method draws some text to the middle of the screen before/after a game
	private void displayTextOnScreen() {
		grpOff.setColor(Color.BLACK);
		strDisplay = "GAME OVER";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4);

		strDisplay = "use the arrow keys to turn and thrust";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 40);

		strDisplay = "use the space bar to fire";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 80);

		strDisplay = "'S' to Start";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 120);

		strDisplay = "'P' to Pause";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 160);

		strDisplay = "'Q' to Quit";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 200);
		strDisplay = "'A' for Shield";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 240);

		strDisplay = "'D' for Teleport";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 280);

		strDisplay = "Collect floaters for power-ups:";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
						+ nFontHeight + 320);

		ShieldFloater shieldFloater = new ShieldFloater(40, new Point(300,
				Game.DIM.height / 4 + nFontHeight + 390));
		shieldFloater.draw(grpOff);

		strDisplay = "Earn Extra Shield";
		grpOff.setColor(Color.BLACK);
		grpOff.drawString(strDisplay, 220, Game.DIM.height / 4 + nFontHeight + 450);

		TeleportFloater teleportFloater = new TeleportFloater(40 , new Point(450,
				Game.DIM.height / 4 + nFontHeight + 390));
		teleportFloater.draw(grpOff);

		strDisplay = "Earn Extra Teleport";
		grpOff.setColor(Color.BLACK);
		grpOff.drawString(strDisplay, 390, Game.DIM.height / 4 + nFontHeight + 450);

		TripleBulletFloater tripleBulletFloater = new TripleBulletFloater(40 , new Point(600,
				Game.DIM.height / 4 + nFontHeight + 390));
		tripleBulletFloater.draw(grpOff);

		strDisplay = "Triple Bullets";
		grpOff.setColor(Color.BLACK);
		grpOff.drawString(strDisplay, 550, Game.DIM.height / 4 + nFontHeight + 450);

		NewShipFloater newShipFloater = new NewShipFloater(40 , new Point(750,
				Game.DIM.height / 4 + nFontHeight + 390));
		newShipFloater.draw(grpOff);

		strDisplay = "Clone Falcon";
		grpOff.setColor(Color.BLACK);
		grpOff.drawString(strDisplay, 700, Game.DIM.height / 4 + nFontHeight + 450);

		LifeFloater lifeFloater = new LifeFloater(40 , new Point(900,
				Game.DIM.height / 4 + nFontHeight + 390));
		lifeFloater.draw(grpOff);

		strDisplay = "Earn Extra Life";
		grpOff.setColor(Color.BLACK);
		grpOff.drawString(strDisplay, 840, Game.DIM.height / 4 + nFontHeight + 450);

		strDisplay = "At the end of each level, fly through the portal to get to the next level.";
		grpOff.drawString(strDisplay, (Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4 + nFontHeight + 510);

	}
	
	public GameFrame getFrm() {return this.gmf;}
	public void setFrm(GameFrame frm) {this.gmf = frm;}

}