package _05dice.P11_22;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by elynn on 10/29/16.
 */
public class TriangleComponent extends JComponent {
    private int mClickCount = 0; // instance variable to keep track of number of clicks

    // instance variables to keep track of x and y coordinates of points
    private int nX1;
    private int nY1;
    private int nX2;
    private int nY2;
    private int nX3;
    private int nY3;

    // constant for dot size
    private static final int DOT_SIZE = 2;

    class ClickListener implements MouseListener {

        public void mouseClicked(MouseEvent e) {
            mClickCount += 1;
            if (mClickCount == 1) {
                // set coordinates for first point
                nX1 = e.getX();
                nY1 = e.getY();
                repaint();
            } else if (mClickCount == 2) {
                // set coordinates for second point
                nX2 = e.getX();
                nY2 = e.getY();
                repaint();
            } else if (mClickCount == 3) {
                // set coordinates for third point
                nX3 = e.getX();
                nY3 = e.getY();
                repaint();
            } else {
                mClickCount = 0; // reset click count
                repaint();
            }
        };

        public void mousePressed(MouseEvent e) {

        };

        public void mouseReleased(MouseEvent e) {

        };

        public void mouseEntered(MouseEvent e) {

        };

        public void mouseExited(MouseEvent e) {

        };
    }

    public TriangleComponent() {
        addMouseListener(new ClickListener());
    }

    public void paintComponent(Graphics g) {
        g.setColor(Color.BLUE);
        if (mClickCount == 1) {
            drawFirstClick(g);
        } else if (mClickCount == 2) {
            drawSecondClick(g);
        } else if (mClickCount == 3) {
            drawThirdClick(g);
        }
        // if mClickCount is not 1, 2, or 3 (i.e. 0), then nothing will be painted and component will be empty
    }

    private void drawFirstClick(Graphics g) {
        g.fillOval(nX1, nY1, DOT_SIZE, DOT_SIZE); // draw the first dot
    }

    private void drawSecondClick(Graphics g) {
        drawFirstClick(g);
        g.fillOval(nX2, nY2, DOT_SIZE, DOT_SIZE); // draw second dot
        g.drawLine(nX1, nY1, nX2, nY2); // draw line connecting first and second dot
    }

    private void drawThirdClick(Graphics g) {
        drawSecondClick(g);
        g.fillOval(nX3, nY3, DOT_SIZE, DOT_SIZE); // draw third dot
        g.drawLine(nX2, nY2, nX3, nY3); // draw line connecting second and third dot
        g.drawLine(nX3, nY3, nX1, nY1); // draw line connecting third and first dot
    }
}
