package _05dice.P11_22;

import javax.swing.*;

/**
 * Created by elynn on 10/29/16.
 */
public class Driver {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent component = new TriangleComponent();
        frame.add(component);

        frame.setVisible(true);
    }
}
