package _05dice.P10_26;

import javax.swing.*;
import java.awt.*;

/**
 * Created by elynn on 10/29/16.
 */
public class OlympicRingsComponent extends JComponent {

    // use constants for width and thickness of ring
    private static final int RING_WIDTH = 60;
    private static final int RING_THICKNESS = 5;

    // constants for the upper-left x and y coordinates of the entire component (i.e. the whole set of rings)
    private static final int X_COORD = 10;
    private static final int Y_COORD = 10;

    public void paintComponent(Graphics g) {
        // draw 3 rings in the top row
        drawRing(g, X_COORD, Y_COORD, Color.BLUE);
        drawRing(g, X_COORD + RING_WIDTH, Y_COORD, Color.BLACK);
        drawRing(g, X_COORD + RING_WIDTH * 2, Y_COORD, Color.RED);

        // draw 2 rings in the bottom row
        drawRing(g, X_COORD + RING_WIDTH / 2, Y_COORD + RING_WIDTH / 2, Color.YELLOW);
        drawRing(g, X_COORD + RING_WIDTH * 3 / 2, Y_COORD + RING_WIDTH / 2, Color.GREEN);
    }

    // call drawRing method to draw each ring, with arguments to specify position and color
    private void drawRing(Graphics g, int nX, int nY, Color colRing) {
        Graphics2D g2 = (Graphics2D) g; // cast to Graphics2D object to set stroke width
        g2.setStroke(new BasicStroke(RING_THICKNESS));
        g2.setColor(colRing);
        g2.drawOval(nX, nY, RING_WIDTH, RING_WIDTH);
    }
}
