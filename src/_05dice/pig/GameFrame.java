package _05dice.pig;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * Created by elynn on 10/30/16.
 */
public class GameFrame extends JFrame {
    private int mPlayerScore = 0; // running tally of player score
    private int mComputerScore = 0; // running tally of computer score
    private int mTurnScore = 0; // points earned so far for the turn currently in progress
    private JLabel mPlayerScoreDisplay; // shows the player's score
    private JLabel mComputerScoreDisplay; // shows the computer's score
    private JLabel mTurnScoreDisplay; // shows the points for the current turn
    private JLabel mMessage; // displays instructions at each stage of game

    // images of die faces
    private ImageIcon mDie1;
    private ImageIcon mDie2;
    private ImageIcon mDie3;
    private ImageIcon mDie4;
    private ImageIcon mDie5;
    private ImageIcon mDie6;
    private JLabel mDieLabel; // displays the appropriate die face
    private int mDieValue; // the value of the current die face

    Random mRand = new Random();

    // buttons for gameplay
    private JButton mRollButton; // player clicks to roll die
    private JButton mPassButton; // player clicks to pass turn

    private boolean mIsPlayerTurn = true; // flag to indicate whose turn it currently is

    private static final int BAD_NUMBER = 1; // the number at which turn ends and score is set to 0
    private static final int POINTS_TO_WIN = 100; // points needed to win game and end play
    private static final int FRAME_WIDTH = 350;
    private static final int FRAME_HEIGHT = 500;
    private static final String PLAYER_TURN_MESSAGE = "Your turn."; // msg shown when player's turn
    private static final String COMPUTER_TURN_MESSAGE = "Computer's turn now."; // msg shown when computer's turn
    private static final String PLAYER_WIN_MESSAGE = "You won!";
    private static final String COMPUTER_WIN_MESSAGE = "Sorry, the computer won.";

    public GameFrame() {
        createScoreComponents(); // scoreboard that shows tally of points so far
        createScorePanel();
        createDieComponent(); // image of die
        createButtonComponents(); // buttons to allow user to roll or pass
        createButtonPanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private void createScoreComponents() {
        mPlayerScoreDisplay = new JLabel("0");
        mPlayerScoreDisplay.setHorizontalAlignment(JLabel.CENTER);
        mPlayerScoreDisplay.setBorder(new TitledBorder(new EtchedBorder(), "Player's Score", TitledBorder.CENTER, 0));
        mComputerScoreDisplay = new JLabel("0");
        mComputerScoreDisplay.setHorizontalAlignment(JLabel.CENTER);
        mComputerScoreDisplay.setBorder(new TitledBorder(new EtchedBorder(), "Computer's Score", TitledBorder.CENTER, 0));
        mTurnScoreDisplay = new JLabel("0");
        mTurnScoreDisplay.setHorizontalAlignment(JLabel.CENTER);
        mTurnScoreDisplay.setBorder(new TitledBorder(new EtchedBorder(), "Current Turn Points", TitledBorder.CENTER, 0));
        mMessage = new JLabel();
        updateMessage(PLAYER_TURN_MESSAGE); // player starts first in the game
        mMessage.setHorizontalAlignment(JLabel.CENTER);
    }

    private void createScorePanel() {
        JPanel scorePanel = new JPanel();
        scorePanel.setLayout(new GridLayout(4,1));
        scorePanel.add(mPlayerScoreDisplay);
        scorePanel.add(mComputerScoreDisplay);
        scorePanel.add(mTurnScoreDisplay);
        scorePanel.add(mMessage);
        add(scorePanel, BorderLayout.NORTH);
    }

    private void createDieComponent() {
        try {
            mDie1 = new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream("./dice-1.png")));
            mDie2 = new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream("./dice-2.png")));
            mDie3 = new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream("./dice-3.png")));
            mDie4 = new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream("./dice-4.png")));
            mDie5 = new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream("./dice-5.png")));
            mDie6 = new ImageIcon(ImageIO.read(this.getClass().getResourceAsStream("./dice-6.png")));
            mDieLabel = new JLabel(mDie6);
            add(mDieLabel, BorderLayout.CENTER);
        } catch (IOException e) {
            System.out.println("Images not found.");
        }
    }

    private void createButtonComponents() {
        mRollButton = new JButton("Roll Die");
        mPassButton = new JButton("Pass Turn");
        mPassButton.setEnabled(false); // player cannot click pass button until they've rolled at least once

        class AddRollListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDieValue(); // throw die and land on random number
                reloadDieImage(); // update die image
                mPassButton.setEnabled(true); // once player has clicked roll button, pass button enabled

                // if die lands on BAD NUMBER, set current turn score to 0, change to computer turn
                if (mDieValue == BAD_NUMBER) {
                    startComputerTurn(); // computer's turn to play
                } else {
                    mTurnScore += mDieValue; // add die value to current turn score
                    updateTurnScoreDisplay(); // update turn score display
                }
            }
        }

        class AddPassListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                mPlayerScore += mTurnScore; // add the points earned so far to player score tally
                updatePlayerScoreDisplay();
                if (mPlayerScore >= POINTS_TO_WIN) {
                    checkScores();
                } else {
                    startComputerTurn(); // if player hasn't won, pass to computer to play
                }
            }
        }

        // add listeners to buttons
        mRollButton.addActionListener(new AddRollListener());
        mPassButton.addActionListener(new AddPassListener());

    }

    private void createButtonPanel() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(mRollButton);
        buttonPanel.add(mPassButton);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    // method updates the current die value to a random number between 1 and 6
    private void updateDieValue() {
        mDieValue = mRand.nextInt(6) + 1;
    }

    // method shows the die face matching the current die value
    private void reloadDieImage() {
        if (mDieValue == 1) {
            mDieLabel.setIcon(mDie1);
        } else if (mDieValue == 2) {
            mDieLabel.setIcon(mDie2);
        } else if (mDieValue == 3) {
            mDieLabel.setIcon(mDie3);
        } else if (mDieValue == 4) {
            mDieLabel.setIcon(mDie4);
        } else if (mDieValue == 5) {
            mDieLabel.setIcon(mDie5);
        } else if (mDieValue == 6) {
            mDieLabel.setIcon(mDie6);
        }

        // animation to simulate roll of die
        Timer timer = new Timer(50, new ActionListener(){
            private int nBounces = 0;

            @Override
            public void actionPerformed(ActionEvent e){
                nBounces += 1;
                if (nBounces == 10) {
                    ((Timer)e.getSource()).stop();
                }
                else if (nBounces % 2 == 0) {
                    mDieLabel.setLocation(mDieLabel.getLocation().x, mDieLabel.getLocation().y + 4);
                } else {
                    mDieLabel.setLocation(mDieLabel.getLocation().x, mDieLabel.getLocation().y - 4);
                }
            }
        });

        timer.start();
    }

    // method throws the die for the computer to simulate its turn
    private void startComputerTurn() {
        mIsPlayerTurn = false;
        buttonsEnabled(false); // disable buttons for player
        updateMessage(COMPUTER_TURN_MESSAGE);
        mTurnScore = 0;
        updateTurnScoreDisplay();


        // timer to set small delay in between computer throws
        Timer timer = new Timer(1000, new ActionListener() {
            private int counter = 0; // counts the number of times computer has thrown die
            private int nTurns = mRand.nextInt(6) + 1; // computer will throw die for a random num of turns, from 1 to 6

            @Override
            public void actionPerformed(ActionEvent e) {
                counter += 1;
                updateDieValue();
                reloadDieImage();
                mTurnScore += mDieValue;
                updateTurnScoreDisplay();
                if (counter == nTurns || mDieValue == BAD_NUMBER) {
                    ((Timer)e.getSource()).stop();
                    if (mDieValue != BAD_NUMBER) {
                        mComputerScore += mTurnScore; // update computer score
                        updateComputerScoreDisplay();
                        checkScores(); // see if computer won
                    }
                    mTurnScore = 0; // reset turn score
                    updateTurnScoreDisplay();
                    mIsPlayerTurn = true;
                    buttonsEnabled(true);
                    updateMessage(PLAYER_TURN_MESSAGE); // player's turn to throw
                }
            }
        });

        timer.start();
    }

    // method to update display of player's score
    private void updatePlayerScoreDisplay() {
        mPlayerScoreDisplay.setText(mPlayerScore + "");
    }

    private void updateComputerScoreDisplay() {
        mComputerScoreDisplay.setText(mComputerScore + "");
    }

    // method to update the display of total points for current turn
    private void updateTurnScoreDisplay() {
        mTurnScoreDisplay.setText(mTurnScore + "");
    }

    private void buttonsEnabled(boolean bEnabled) {
        if (bEnabled) {
            mRollButton.setEnabled(true);
            mPassButton.setEnabled(false); // pass button only enabled when roll button clicked once
        } else {
            mRollButton.setEnabled(false);
            mPassButton.setEnabled(false);
        }
    }

    // updates message shown to indicate whose turn it is / whether game is over
    private void updateMessage(String strMessage) {
        mMessage.setText(strMessage);
        if (strMessage.equals(PLAYER_TURN_MESSAGE) || strMessage.equals(PLAYER_WIN_MESSAGE)) {
            mMessage.setForeground(Color.BLUE);
        } else if (strMessage.equals(COMPUTER_TURN_MESSAGE) || strMessage.equals(COMPUTER_WIN_MESSAGE)) {
            mMessage.setForeground(Color.RED);
        }
    }

    // method checks if either player has enough points to win
    private void checkScores() {
        if (mPlayerScore >= POINTS_TO_WIN) {
            updateMessage(PLAYER_WIN_MESSAGE);
            restartGame();
        } else if (mComputerScore >= POINTS_TO_WIN) {
            updateMessage(COMPUTER_WIN_MESSAGE);
            restartGame();
        }
    }

    // ask user if they want to play again
    private void restartGame() {
        int reply = JOptionPane.showConfirmDialog(null, "Would you like to play again?", "Continue Game?", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            mPlayerScore = 0;
            mComputerScore = 0;
            mTurnScore = 0;
            updatePlayerScoreDisplay();
            updateComputerScoreDisplay();
            updateTurnScoreDisplay();
            mIsPlayerTurn = true;
            buttonsEnabled(true);
            updateMessage(PLAYER_TURN_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Thanks for playing Pig!");
            System.exit(0);
        }
    }

}
