package _05dice.P10_2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class ButtonFrame1 extends JFrame
{
    private static final int FRAME_WIDTH = 100;
    private static final int FRAME_HEIGHT = 60;
    private int mClicks = 0; // store num of clicks as an instance variable

    // implement ClickListener as a private class so that it can access instance variables
    public class ClickListener implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            mClicks += 1;

            // print message with num of clicks so far
            if (mClicks == 1) {
                System.out.println("I was clicked 1 time."); // separate condition because 'time' is singular for 1
            } else {
                System.out.println("I was clicked " + mClicks + " times.");
            }
        }
    }

    public ButtonFrame1()
    {
        createComponents();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    private void createComponents()
    {
        JButton button = new JButton("Click me!");
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel);

        ActionListener listener = new ClickListener();
        button.addActionListener(listener);
    }
}
