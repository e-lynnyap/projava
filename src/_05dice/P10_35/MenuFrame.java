package _05dice.P10_35;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by elynn on 10/29/16.
 */
public class MenuFrame extends JFrame {
    // instance variables for ten meal buttons
    private JButton mMeal1Button;
    private JButton mMeal2Button;
    private JButton mMeal3Button;
    private JButton mMeal4Button;
    private JButton mMeal5Button;
    private JButton mMeal6Button;
    private JButton mMeal7Button;
    private JButton mMeal8Button;
    private JButton mMeal9Button;
    private JButton mMeal10Button;

    // instance variables for text fields to add custom item
    private JTextField mCustomItemName;
    private JTextField mCustomItemPrice;
    private JButton mCustomItemButton;

    // instance variables for results area
    private JLabel mCostLabel = new JLabel("Cost: ");
    private JLabel mTaxLabel = new JLabel("Tax: ");
    private JLabel mTipLabel = new JLabel("Tip: ");
    private JLabel mTotalLabel = new JLabel("Total: ");
    private JLabel mCostDisplay = new JLabel("$0.00");
    private JLabel mTaxDisplay = new JLabel("$0.00");
    private JLabel mTipDisplay = new JLabel("$0.00");
    private JLabel mTotalDisplay = new JLabel("$0.00");

    // instance variables to keep track of bill, tax and tip
    private double mCost = 0;
    private double mTax = 0;
    private double mTip = 0;
    private double mTotal = 0;

    // use constants for frame dimensions
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 300;

    public MenuFrame() {
        createButtons();
        createButtonPanel();
        createTextComponents();
        createTextPanel();
        createBillPanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private void createButtons() {
        // create buttons for 10 meals
        mMeal1Button = new JButton("Big Mac");
        mMeal2Button = new JButton("McNuggets");
        mMeal3Button = new JButton("Filet O'Fish");
        mMeal4Button = new JButton("Quarter Pounder");
        mMeal5Button = new JButton("Cheeseburger");
        mMeal6Button = new JButton("Double Cheeseburger");
        mMeal7Button = new JButton("Regular Fries");
        mMeal8Button = new JButton("Large Fries");
        mMeal9Button = new JButton("Apple Pie");
        mMeal10Button = new JButton("McFlurry");

        class AddItemListener implements ActionListener {
            private double mPrice;

            public AddItemListener(double dPrice) {
                mPrice = dPrice;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                mCost += mPrice;
                updateBill();
            }
        }

        // add listeners to buttons
        mMeal1Button.addActionListener(new AddItemListener(3.99));
        mMeal2Button.addActionListener(new AddItemListener(4.49));
        mMeal3Button.addActionListener(new AddItemListener(3.79));
        mMeal4Button.addActionListener(new AddItemListener(3.79));
        mMeal5Button.addActionListener(new AddItemListener(1.00));
        mMeal6Button.addActionListener(new AddItemListener(1.69));
        mMeal7Button.addActionListener(new AddItemListener(1.69));
        mMeal8Button.addActionListener(new AddItemListener(1.89));
        mMeal9Button.addActionListener(new AddItemListener(0.99));
        mMeal10Button.addActionListener(new AddItemListener(2.39));

    }

    private void createTextComponents() {
        // create text fields and button for adding custom dish
        mCustomItemName = new JTextField("Enter item name", 10);
        mCustomItemPrice = new JTextField("Enter item price", 10);
        mCustomItemButton = new JButton("Add");

        class AddDishListener implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                mCost += getDishPrice();
                updateBill();
            }
        }

        mCustomItemButton.addActionListener(new AddDishListener());
    }

    private void createButtonPanel() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(5, 2));
        buttonPanel.add(mMeal1Button);
        buttonPanel.add(mMeal2Button);
        buttonPanel.add(mMeal3Button);
        buttonPanel.add(mMeal4Button);
        buttonPanel.add(mMeal5Button);
        buttonPanel.add(mMeal6Button);
        buttonPanel.add(mMeal7Button);
        buttonPanel.add(mMeal8Button);
        buttonPanel.add(mMeal9Button);
        buttonPanel.add(mMeal10Button);
        add(buttonPanel, BorderLayout.NORTH);
    }

    private void createTextPanel() {
        JPanel textPanel = new JPanel();
        textPanel.add(mCustomItemName);
        textPanel.add(mCustomItemPrice);
        textPanel.add(mCustomItemButton);
        add(textPanel, BorderLayout.CENTER);
    }

    private void createBillPanel() {
        JPanel billPanel = new JPanel();
        billPanel.setLayout(new FlowLayout());
        JPanel containerPanel = new JPanel();
        containerPanel.setLayout(new GridLayout(4,2));
        containerPanel.add(mCostLabel);
        containerPanel.add(mCostDisplay);
        containerPanel.add(mTaxLabel);
        containerPanel.add(mTaxDisplay);
        containerPanel.add(mTipLabel);
        containerPanel.add(mTipDisplay);
        containerPanel.add(mTotalLabel);
        containerPanel.add(mTotalDisplay);
        billPanel.add(containerPanel, BorderLayout.CENTER);
        add(billPanel, BorderLayout.SOUTH);
    }

    // helper method to update bill displayed
    private void updateBill() {
        mCostDisplay.setText("$" + String.format("%.2f", mCost));
        mTax = mCost * 0.1025; // update total tax
        mTip = mCost * 0.15; // update total tip
        mTotal = mCost + mTax + mTip;
        mTaxDisplay.setText("$" + String.format("%.2f", mTax));
        mTipDisplay.setText("$" + String.format("%.2f", mTip));
        mTotalDisplay.setText("$" + String.format("%.2f", mTotal));
    }

    // helper method to validate and retrieve amount entered in dish price field
    private double getDishPrice() {
        try {
            return Double.parseDouble(mCustomItemPrice.getText());
        } catch (NumberFormatException nfe) {
            // show an error message if user tries to add non-numeric value to bill
            JOptionPane.showMessageDialog(null, "You must enter a numeric value for the price of the item.");
        }
        return 0;
    }
}
