package _02arrays;
import java.util.*;
import java.io.*;

/**
 * Created by elynn on 10/7/16.
 */
public class P7_17 {
    public static void main(String[] args) {

        try {
            // create Scanner object for input file
            File inputFile = new File("sales.txt");
            Scanner in = new Scanner(inputFile);

            // create arraylist to store service categories
            ArrayList<String> strOutputNames = new ArrayList<>();

            // create array to store details on each line except service category
            String[] strDetails;

            // create variable for file name based on service category
            String strOutputName;

            String strLine;
            // read each line in input file
            while (in.hasNextLine()) {
                strLine = in.nextLine();
                strDetails = strLine.split(";");
                strOutputName = strDetails[1] + ".txt";
                PrintWriter output;

                // if file for category already exists, create printwriter for existing file
                if (strOutputNames.contains(strOutputName)) {
                    output = new PrintWriter(new BufferedWriter(new FileWriter(strOutputName, true)));
                } else { // otherwise, create a new file for category and add name to arraylist
                    strOutputNames.add(strOutputName);
                    output = new PrintWriter(strOutputName);
                }

                // write details to the file
                output.println(strDetails[0] + ";" + strDetails[2] + ";" + strDetails[3]);
                output.close();
            }

            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("Input file does not exist.");
        } catch (NoSuchElementException e) {
            System.out.println("File formatting cannot be read.");
        } catch (IOException e) {
            System.out.println("Error writing file.");
        }
    }
}
