package _02arrays;
import java.util.Random;

/**
 * Created by elynn on 10/7/16.
 */
public class P6_12 {
    public static void main(String[] args) {
        // generate array of 20 random die tosses
        int[] nTosses = new int[20];
        Random rand = new Random();
        for (int nC = 0; nC < 20; nC++) {
            nTosses[nC] = rand.nextInt(6) + 1;
        }

        Boolean bInRun = false;

        for (int nC = 0; nC < 20; nC++) {
            if (bInRun) {
                if (nTosses[nC] != nTosses[nC - 1]) {
                    System.out.print(")");
                    bInRun = false;
                }
            }
            if (!bInRun) {
                if (nC != 19 && nTosses[nC] == nTosses[nC + 1]) {
                    System.out.print("(");
                    bInRun = true;
                }
            }
            System.out.print(nTosses[nC]);
        }

        if (bInRun) {
            System.out.println(")");
        }
    }
}
