package _02arrays;
import java.util.*;

/**
 * Created by elynn on 10/7/16.
 */
public class P6_23 {
    public static void main(String[] args) {
        // pseudocode
        // initialize arraylist for values and captions
        // while true
            // ask for caption
            // if user types 'q'
                // break
            // else
                // add caption to captions
            // ask for data value
            // add value to values
        // find maximum value
        // find longest string
        // for each index in strCaptions
            // print element at index in strCaptions, aligned acc to length of longest string
            // print element at index in intValues

        Scanner scan = new Scanner(System.in);
        ArrayList<Double> douValues = new ArrayList<>();
        ArrayList<String> strCaptions = new ArrayList<>();

        String strInput;
        do {
            System.out.println("Enter a caption for your value. Type 'q' to quit.");
            strInput = scan.next();
            if (strInput.equals("q")) {
                break;
            } else {
                strCaptions.add(strInput);
            }

            System.out.println("Enter your value.");
            douValues.add(scan.nextDouble());
        } while (true);

        double dMax = Collections.max(douValues);
        int nMaxStringWidth = 0;
        for (String strCaption : strCaptions) {
            if (strCaption.length() > nMaxStringWidth) {
                nMaxStringWidth = strCaption.length();
            }
        }
        nMaxStringWidth += 1;

        int nAsteriskCount;
        for (int nC = 0; nC < strCaptions.size(); nC++) {
            System.out.printf("%" + nMaxStringWidth + "s", strCaptions.get(nC) + " ");
            nAsteriskCount = (int) Math.round(douValues.get(nC) / dMax * 40);
            for (int nD = 0; nD < nAsteriskCount; nD++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
