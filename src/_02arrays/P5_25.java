package _02arrays;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Created by elynn on 10/7/16.
 */
public class P5_25 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a zip code (5 integer digits only): ");
        int nZipCode = scan.nextInt();
        printBarCode(nZipCode);
    }

    public static void printDigit(int nD) {
        switch (nD){
            case 1:
                System.out.print(":::||");
                return;
            case 2:
                System.out.print("::|:|");
                return;
            case 3:
                System.out.print("::||:");
                return;
            case 4:
                System.out.print(":|::|");
                return;
            case 5:
                System.out.print(":|:|:");
                return;
            case 6:
                System.out.print(":||::");
                return;
            case 7:
                System.out.print("|:::|");
                return;
            case 8:
                System.out.print("|::|:");
                return;
            case 9:
                System.out.print("|:|::");
                return;
            default:
                System.out.print("||:::");
        }
    }

    public static void printBarCode(int nZipCode) {
        // print first full bar
        // convert zipcode to array of digits
        // print digit for each digit in array
        // calculate check digit
        // print digit for check digit
        // print last full bar
        System.out.print("|");

        String strTemp = Integer.toString(nZipCode);
        int[] nDigits = new int[strTemp.length()];
        for (int nC = 0; nC < strTemp.length(); nC++) {
            nDigits[nC] = strTemp.charAt(nC) - '0';
        }

        for (int nDigit : nDigits) {
            printDigit(nDigit);
        }

        int nSumOfZipDigits = IntStream.of(nDigits).sum();
        int nCheckDigit = ((nSumOfZipDigits + 9) / 10) * 10 - nSumOfZipDigits;

        printDigit(nCheckDigit);
        System.out.print("|");
    }

}
