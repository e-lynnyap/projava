package _02arrays;
import java.util.*;

/**
 * Created by elynn on 10/7/16.
 */
public class P6_28 {
    public static void main(String[] args) {
        ArrayList<Integer> intAValues = new ArrayList<>();
        intAValues.add(1);
        intAValues.add(4);
        intAValues.add(9);
        intAValues.add(16);
        ArrayList<Integer> intBValues = new ArrayList<>();
        intBValues.add(4);
        intBValues.add(7);
        intBValues.add(9);
        intBValues.add(9);
        intBValues.add(11);
        System.out.println(mergeSorted(intAValues, intBValues));
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> intAValues,
                                                 ArrayList<Integer> intBValues) {
        ArrayList<Integer> intMergedValues = new ArrayList<>();

        int nAIndex = 0;
        int nBIndex = 0;
        int nALength = intAValues.size();
        int nBLength = intBValues.size();

        // compare current head of each array and add smaller value to merged list
        while (nAIndex < nALength && nBIndex < nBLength) {
            if (intAValues.get(nAIndex) <= intBValues.get(nBIndex)) {
                intMergedValues.add(intAValues.get(nAIndex));
                nAIndex += 1;
            } else {
                intMergedValues.add(intBValues.get(nBIndex));
                nBIndex += 1;
            }
        }

        // add any remaining elements in either array
        if (nAIndex < nALength) {
            for (int nC = nAIndex; nC < nALength; nC++) {
                intMergedValues.add(intAValues.get(nC));
            }
        } else if (nBIndex < nBLength) {
            for (int nC = nBIndex; nC < nBLength; nC++) {
                intMergedValues.add(intBValues.get(nC));
            }
        }

        return intMergedValues;
    }
}
