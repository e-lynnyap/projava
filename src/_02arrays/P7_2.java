package _02arrays;
import java.util.*;
import java.io.*;

/**
 * Created by elynn on 10/7/16.
 */
public class P7_2 {
    public static void main(String[] args) throws FileNotFoundException {
        // ask user for input file name
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the name of your input file: ");
        String strInputName = scan.next();

        // ask user for output file name
        System.out.println("Enter the name of your output file: ");
        String strOutputName = scan.next();

        // create scanner and printer
        File inputFile = new File(strInputName);
        Scanner in = new Scanner(inputFile);
        PrintWriter out = new PrintWriter(strOutputName);

        // read input and write to file
        int nLine = 1;
        while (in.hasNextLine()) {
            out.println("/* " + nLine + " */ " + in.nextLine());
            nLine += 1;
        }

        // close scanner and printwriter
        in.close();
        out.close();
    }
}
