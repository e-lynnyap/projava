package _02arrays;
import java.util.*;


/**
 * Created by elynn on 10/7/16.
 */
public class P5_20 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a year (integer values only): ");
        int nYear = scan.nextInt();
        if (isLeapYear(nYear)) {
            System.out.println(nYear + " is a leap year.");
        } else {
            System.out.println(nYear + " is not a leap year.");
        }
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 != 0) {
            return false;
        }

        if (year <= 1582) {
            return true;
        }

        if (year % 400 == 0) {
            return true;
        }

        if (year % 100 == 0) {
            return false;
        } else {
            return true;
        }

    }
}
