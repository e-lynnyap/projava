package _02arrays;

import java.util.*;

/**
 * Created by elynn on 10/7/16.
 */
public class P5_8 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a sentence: ");
        while (scan.hasNext()) {
            System.out.print(scramble(scan.next()) + " ");
        }
    }

    public static String scramble(String strWord) {
        // pseudocode
        // convert word to array of characters
        // counter letters in word
        // if word has fewer than four letters, return word
        // choose two distinct integers a, b in the range 1..word.length - 2
        // make sure letters[a] and letters[b] are letters (not punctuation) and distinct from each other
        // swap letters letters[a] and letters[b]
        // return word
        char[] cLetters = strWord.toCharArray();

        if (countLetters(cLetters) < 4) {
            return strWord;
        }

        Random rand = new Random();
        int nA;
        int nB;

        do {
            nA = rand.nextInt(cLetters.length - 2) + 1;
        } while (!Character.isLetter(cLetters[nA]));
        do {
            nB = rand.nextInt(cLetters.length - 2) + 1;
        } while (nB == nA || !Character.isLetter(cLetters[nB]));

        char cTemp = cLetters[nA];
        cLetters[nA] = cLetters[nB];
        cLetters[nB] = cTemp;

        return new String(cLetters);
    }

    public static int countLetters(char[] cLetters){
        int nCount = 0;
        for (char cLetter : cLetters) {
            if (Character.isLetter(cLetter)) {
                nCount += 1;
            }
        }
        return nCount;
    }
}
